function MAE6780HW2Q3
% Eileen J. Kim (ejk93)
% MAE 6780 HW # 3, Q # 3a

%% Problem Set-Up
A = [0 1 0; 
    980 0 -2.8; 
    0 0 -100];
B = [0 0 100]';
Cz = [10 1 0; 
    0 0 0];
Dzu = [0 1]';

%% Pole Placement
p = [-30 -15 -3]; % Arbitrary (Stable) Poles
F = place(A,B,p);

%% Cost Calculation
Q = (Cz + Dzu*F)'*(Cz + Dzu*F);
Af = A + B*F;
X = lyap(Af', Q);
x0 = [0.1 0 0]'; % Arbitrary initial condition
gamma1 = x0'*X*x0


