function March14ex
% See MAE 6780 lecture notes for 3/14/14, example 1

% run program, then type in Pbig, Gsys, Ksys, or CLsys in MATLAB workspace
clear all
close all
clc


Gsys = zpk([], 0, 1); % G = 1/s;
%zpk: zero-pole-gain
Ksys = zpk([], [], 1); % K = 1

%% OPEN LOOP SYSTEM PBIG
systemnames = 'Gsys';
inputvar = '[r{1};u{1}]'; %number in curly brackets tell you MIMO OR SISO, in this case - SISO
outputvar = '[r-Gsys; r-Gsys]'; % {z1;y}
input_to_Gsys = '[u]';
sysoutname = 'Pbig'; cleanupsysic = 'yes'; sysic

%% CLOSED LOOP SYSTEM
systemnames = 'Gsys Ksys';
inputvar = '[r{1}]'; % no longer need u (as opposed to OL sys)
outputvar = '[r-Gsys]'; % {z1}
input_to_Gsys = '[Ksys]'; %input to G is Ksys (which is the output of K)
input_to_Ksys = '[r-Gsys]';
sysoutname = 'CLsys'; cleanupsysic = 'yes'; sysic

%type 1 system: pole at s = 0, zero-tracking error

%% CLOSED LOOP SYSTEM METHOD 2
CL2sys = lft(Pbig, Ksys);