\documentclass[11pt]{article} % use larger type; default would be 10pt

\usepackage[utf8]{inputenc} % set input encoding (not needed with XeLaTeX)

\usepackage[framed, numbered]{mcode} % Allow inclusion of MATLAB code
%%% PAGE DIMENSIONS
\usepackage{geometry}
\geometry{letterpaper}
\geometry{margin=1in}

\usepackage{graphicx}
\usepackage{caption}
\usepackage{subcaption}
\usepackage{float}
\usepackage{amsmath, amssymb,graphics}
\usepackage{mathrsfs}
 
%%% PACKAGES
\usepackage{booktabs}
\usepackage{array}
\usepackage{paralist} 
\usepackage{verbatim}
\usepackage{subfig}
\usepackage{stmaryrd}
\usepackage{mathrsfs}

%%% HEADERS & FOOTERS
\usepackage{fancyhdr} 
\pagestyle{fancy}
\renewcommand{\headrulewidth}{0.2pt} 
\setlength{\headheight}{26pt}
\pagenumbering{arabic}
\lhead{Eileen Kim, ejk93\\
May 8, 2014}
\chead{}
\rhead{\thepage}
\lfoot{}\cfoot{\thepage}\rfoot{}

%%% SECTION TITLE APPEARANCE
\usepackage{sectsty}
\allsectionsfont{\sffamily\mdseries\upshape}

\begin{document}
\begin{center}
\LARGE{\textbf{MAE 6780: Homework 3}}\\
\end{center}

\begin{enumerate}
%% Question 1
\item \textbf{Frequency-Weighted Noise}
After detrending the data to shift it to a zero-mean data set, we can observe the following plot, which contains both the de-trended and original data. 
			\begin{figure}[H]
			\centering
			\includegraphics[scale = 0.8, trim = 1.5in 3.25in 1.5in 3.25in, clip]{Q1a_detrend1}
%			\caption{EPA Driving Cycle Data}
			\end{figure} 
	\begin{enumerate}
		% Part 1.a: 
		\item To plot the power spectral intensity of the target speed $r(t)$, we first take the Fourier transform of the signal (the driving cycle data) and normalize it by the sampling time by using the MATLAB function \mcode{ddCycle_fft = fft(ddCycle_detrend)*dt}. With the Fourier transform, we can then apply the Lemma definition of the power spectral intensity:
			\begin{equation*}
				S_{xx}(\omega) = \mathbb{E}[X(j\omega)^*X(j\omega)]
			\end{equation*}
			where $X(j\omega)$ represents the Fourier transform of the signal $x(t)$:
			\begin{equation*}
			X(j\omega) = \mathscr{F}[x(t)]
			\end{equation*}
		Due to foldback aliasing which occurs during the discrete Fast Fourier Transform, we only apply the Lemma 10 definition to half of the sample using a \mcode{for} loop (dividing by the final time in order to get the expected value of the transform):
			\begin{lstlisting}
for i = 1:n
    % Apply the Lemma throughout half of the sample
    Sxx(i,:,:) = (ddCycle_fft(i,:))'*ddCycle_fft(i,:)/T(end);
end
			\end{lstlisting}
		Using these values, we can create the plot for power spectral density as follows:
			\begin{figure}[H]
		 	\centering
		 	\includegraphics[scale = 0.8, trim = 1.5in 3.25in 1.5in 3.25in, clip]{Q1a_PSD1}
%		 	\caption{Power Spectral Density}
		 	\end{figure} 
		 	
		% Part 1.b
		\item To fit a frequency weighting $W_r(s)$ subject to unit intensity white noise $w(t)$, we look at the power spectral density of the original data (we can look at the graph produced in the previous part of this question) in the log-log frame, and, using the methods of drawing Bode plots of transfer functions, formulate a transfer function $W_r(s)$ to fit the general shape. This requires a little bit of trial-and-error, but results in the transfer function:
			\begin{equation*}
			\begin{split}
			W_r(s) = & k\left(\frac{s}{10^{-1}}+1\right)^{-2}\\
			\textrm{where }& k = 10^{1.8}
			\end{split}
			\end{equation*}
		The transfer function is mapped to $s = j\omega$ (we can ignore the $\sigma$ because we're only considering steady-state frequency behavior, ignoring the attenuation component/transient behavior) with a covariance, $C_{ww}$, of 1 which stems from the problem's specification of a unit intensity white noise, $w(t)$. The power spectral intensity, according to Lemma 11, is: 
			\begin{equation*}
			S_{xx} = W_r(j\omega)*C_{ww}*W_r(j\omega)^{*}
			\end{equation*}
		which, plotted with the data's power spectral density, results in the following figure:
			\begin{figure}[H]
		 	\centering
			\includegraphics[scale = 0.8, trim = 1.5in 3.25in 1.5in 3.25in, clip]{Q1b_Comparison1}
%			\caption{Power Spectral Density}
			\end{figure} 	
		As can be seen, the two graphs match up fairly well. (Yay!)
		
		% Part 1.c
		\item Below is a comparison of a time-domain simulation of the frequency weighted noise fit derived in the previous part with the EPA driving cycle data. 
			\begin{figure}[H]
			\centering
			\includegraphics[scale = 0.8, trim = 1.5in 3.25in 1.5in 3.25in, clip]{Q1c}
			\end{figure} 
		The graphs don't look at all similar at first glance but the timing of the movement of the two simulations tend to match up. This may be because we matched the power density curves (which were mapped in the frequency domain)to each other, so the frequency of the behavior is very similar. However, the magnitude of the curves don't match. This is to be expected because the white noise simulation (\mcode{w = randn(n,1)}) is completely random and yields different values each time the MATLAB program is run. There are some times when the simulations may match each other better than other times, but it's generally completely random.
	\end{enumerate}

%% Question 2
\item \textbf{Linear Quadratic Gaussian Control}
\begin{equation*}
	G = \begin{cases}
	\dot{x}(t) = Ax(t) + B_uu(t)\\
	v(t) = C_vx(t) + D_{vu}u(t)
	\end{cases}
\end{equation*}
where
\begin{equation*}
A = 
\begin{bmatrix}
	-0.8409 & -9.699 & 0\\
	0.1803 & 0.8461 & -0.5271\\
	-2.03 & 2440 & -28.01
\end{bmatrix}
B_u = 
\begin{bmatrix}
	29.15 & -22.11\\
	0.01547 & 6.976\\
	0.02799 & -11.54
\end{bmatrix}
C_v = 
\begin{bmatrix}
	1 & 0 & 0\\
	0 & 0 & 1
\end{bmatrix}
D_{vu} = 
\begin{bmatrix}
	0
\end{bmatrix}
\end{equation*}
where
\begin{equation*}
\begin{split}
u &= [\gamma \textrm{  } \alpha]^{*}\\
v &= [\omega_e \textrm{  } \omega_m]^{*}
\end{split}
\end{equation*}
	\begin{enumerate}
	% Part 2.a
	\item Formulating the general control problem:
	\begin{equation*}
	\begin{split}
	T_{zw}(s) &= LFT(P(s),K(s))\\
	\begin{bmatrix}
	z\\
	y
	\end{bmatrix} &= P(s) \begin{bmatrix}
	w\\
	u
	\end{bmatrix}\\
	u &= K(s)y
	\end{split}
	\end{equation*}
	
		\begin{enumerate}
		% Part 2.a.i
		\item Defining the signals based on the problem description: $z$ is the regulated 'error' signal (what we'd like to keep small), $y$ is the measured signal (values which are available to our controller), $w$ is the exogenous signal (what we don't control), which in this problem is a white noise signal. We set $z_1$ as the regulated tracking error and $z_2$ as the regulated control effort. $y_1$ is designated as $r_e - v_1 = r_e - \omega_e$, and $y_2$ as $r_m - v_2 = r_m - \omega_m$.    
			\begin{equation*}
			\begin{split}
			r &= W_rw = \begin{bmatrix}
			W_mW_e\\
			W_m
			\end{bmatrix}w\\
			y &= r - v = W_rw-(C_vx + D_{vu}u)\\
			z &= \begin{bmatrix}
			z_1\\
			z_2
			\end{bmatrix} = \begin{bmatrix}
			W_p & 0\\
			0 & W_u
			\end{bmatrix} \begin{bmatrix}
			y\\
			u
			\end{bmatrix} = \begin{bmatrix}
			W_py\\
			W_uu
			\end{bmatrix} = \begin{bmatrix}
			-W_pC_vx - W_pD_{vu}u + W_pW_rw\\
			W_uu
			\end{bmatrix}
%			\begin{bmatrix}
%				z\\
%				y
%				\end{bmatrix} &= P(s) \begin{bmatrix}
%				w\\
%				u
%				\end{bmatrix} = P(s)\begin{bmatrix}
%				w\\
%				\gamma\\
%				\alpha
%				\end{bmatrix}\\
%			\begin{bmatrix}
%			\omega_e\\
%			\omega_m
%			\end{bmatrix} &= Cx + D\begin{bmatrix}
%			\gamma\\
%			\alpha
%			\end{bmatrix}\\
%			r = \begin{bmatrix}
%			r_e\\
%			r_m
%			\end{bmatrix} &= \begin{bmatrix}
%			W_mW_e\\
%			W_m
%			\end{bmatrix}
			\end{split}
			\end{equation*}
			\begin{figure}[H]
			\centering
			\includegraphics[scale = 0.5, trim = 0in 0in 0in 0in, clip]{Q2aipdf}
%			\caption{System Block Diagram}
			\end{figure}
		\item Find P matrix symbolically
		%Part 2.a.ii
		\begin{equation*}
		\begin{split}
			\begin{bmatrix}
			\dot{x}\\ z\\ y
			\end{bmatrix} & = \begin{bmatrix}
			A & B_w & B_u\\
			C_z & D_{zw} & D_{zu}\\
			C_y & D_{yw} & D_{yu}
			\end{bmatrix} \begin{bmatrix}
			x\\ w\\ u
			\end{bmatrix}\\
			\begin{bmatrix}
			\dot{x}\\ z_1\\ z_2\\y
			\end{bmatrix} & = \begin{bmatrix}
			A & 0 & B_u\\
			-W_pC_v & W_p\begin{bmatrix} W_mW_e\\ W_m\end{bmatrix} & -W_pD_{vu}\\
			0 & 0 & W_u\\
			C_v & \begin{bmatrix} W_mW_e\\ W_m\end{bmatrix}  & -D_{vu}
			\end{bmatrix} \begin{bmatrix}
			x\\ w \\ u
			\end{bmatrix}\\
		z_1 &= W_py = W_p(r - v) = W_p(r - Gu)\\
		z_2 &= W_uu\\
		y_1 &= r(1) - v(1) = r(1) - Gu(1)\\
		y_2 &= r(2) - v(2) = r(2) - Gu(2)\\
		\end{split}
		\end{equation*}
		\begin{equation*}
			\begin{bmatrix}
			z_1\\ z_2\\y_1\\y_2
			\end{bmatrix}  = \begin{bmatrix}
			& W_p\begin{bmatrix} W_e \\ I \end{bmatrix} & & -G \begin{bmatrix} W_e \\ I \end{bmatrix} \\
			1 & 0 & -G & 0\\
			0 & 1 & 0 & -G \end{bmatrix} \begin{bmatrix}
			\begin{bmatrix} r_e \\ r_m \end{bmatrix} \\ \begin{bmatrix} \gamma \\ \alpha \end{bmatrix}			\end{bmatrix}			
		\end{equation*}	
		
		%Part 2.a.iii
		\item Problem formulation in \mcode{sysic} \\
		We need to also add in $W_n$, a frequency weighted noise input in order to add noise to our "perfect" measurements and be able to solve the system for the controller in later parts of this problem. At this point in the problem, $W_p$ and $W_u$ are designated as identity matrices but will be adjusted later on. 
		\begin{lstlisting}
% Frequency Weightings
We = 1/(s + 0.5); % given engine speed weighting
km = 10^(1.8); 
Wm = zpk(km*(s/(1e-1)+1)^(-2)); % from problem 1
Wp = eye(2);
Wu = eye(2);
Wn = 0.001*eye(2);
Wr = [Wm*We; Wm];

% Sysic
systemnames = 'G Wp Wu Wr Wn';
inputvar = '[w{3};u{2}]';
outputvar = '[Wp; Wu; Wr-G-Wn]'; %[z1; z2; y]
input_to_G = '[u]';
input_to_Wp = '[Wr-G]';
input_to_Wu = '[u]';
input_to_Wr = '[w(1)]';
input_to_Wn = '[w(2:3)]';
sysoutname = 'P'; cleanupsysic = 'yes'; sysic;
		\end{lstlisting}
		\end{enumerate}
		
	% Part 2.b.
	\item In order to tune the weighting frequencies, we first try to adjust them such that the outputs $(z_1, z_2)$ are of unit intensity. The problem statement tells us that the tracking error of $r_e$, $eT_e$ is 0.1, and that the tracking error of $r_m$, $eT_m$ is 0.2. Since $W_p$ affects these tracking errors, we adjust $W_p$ to be
	\begin{equation*}
	W_p = 
	\begin{bmatrix}
	\frac{1}{eT_e} & 0\\
	0 & \frac{1}{eT_m}	
	\end{bmatrix} = \begin{bmatrix}
	10 & 0 \\
	0 & 5
	\end{bmatrix}
	\end{equation*}
	Similarly, we are told that $|\gamma| \leq 10$ and $|\alpha| \leq 2.5$, so we determine $W_u$ to be
	\begin{equation*}
	W_u = 
	\begin{bmatrix}
		\frac{1}{\gamma_{\mathrm{max}}} & 0\\
		0 & \frac{1}{\alpha_{\mathrm{max}}}
	\end{bmatrix} = \begin{bmatrix}
	0.1 & 0\\
	0 & 0.4
	\end{bmatrix}
	\end{equation*}
%	Note that $W_u(s)$ is multiplied by $10^(1.5)$. This is in order to get better satisfaction of our closed loop specifications. 
After tuning these weighting frequencies, we can use the MATLAB function \mcode{h2syn} to synthesize an $H_2$ controller. 
	\begin{lstlisting}
nmeas = 2; ncon = 2;
[K, CL, GAM, INFO] = h2syn(P,nmeas,ncon);
	\end{lstlisting}
	We can check that this controller works by verifying that $\gamma \leq 1$. In our case, $\gamma = 0.747$. 
	
	% Part 2.c.
	\item Verifying the closed loop specifications
		\begin{enumerate}
		% Part 2.c.i.
		\item $\Phi_{uw}$ is the closed loop transfer function from $w$ to $u$.
			\begin{enumerate}
				% Part 2.c.i.A.
				\item We can find the transfer function $\Phi_{uw}$ by drawing the appropriate block diagram and using \mcode{sysic}. Below is the block diagram we need. 
				\begin{figure}[H]
				\centering
				\includegraphics[scale = 0.5, trim = 0in 0in 0in 0in, clip]{Q2aipdf}
				%\caption{System Block Diagram}
				\end{figure}
				This translates into the following \mcode{sysic} model. 
				\begin{lstlisting}
% Transfer function from r to u
sys_ur = feedback(K,G);

% Transfer function from w to u (sysic)
systemnames = 'sys_ur Wr';
inputvar = '[w{1}]';
outputvar = '[sys_ur]'; %[z1; z2; y]
input_to_sys_ur = '[Wr]';
input_to_Wr = '[w]';
sysoutname = 'Phi_uw'; cleanupsysic = 'yes'; sysic;
				\end{lstlisting}
				Using $\Phi_{uw}$ and the following set of equations and properties, we can analytically solve for the covariance of $u$. 
				\begin{equation*}
					\Phi_{uw} = \left\{
							\begin{array}{l}
							\dot{x}(t) = Ax(t) + B_uu(t)\\
							v(t) + C_vx(t) + D_{vu}u(t)
							\end{array}\right\}\\	
				\end{equation*}
				\begin{equation*}
				\begin{split}
					u &= \mathbb{E}[uu^{*}]\\
					& = \mathbb{E}[(Cx + Dw)(Cx + Dw)^{*}]\\	
					&= \mathbb{E}[(Cx)(Cx)^{*}]\\
					&= C\mathbb{E}[xx^{*}]C^{*}\\
					&= C\mathbb{E}\mcode{lyap(A, B^{*}B)}C^{*}			
				\end{split}
				\end{equation*}
				which can be modeled and calculated in \mcode{sysic} as:
				\begin{lstlisting}
% Analytically calculating covariance of u
cov_u = Phi_uw.c*lyap(Phi_uw.a,Phi_uw.b*(Phi_uw.b'))*Phi_uw.c';

% Verifying our analytical cov(u)
covu1 = cov(y(:,3)); % u1
covu2 = cov(y(:,4)); % u2
				\end{lstlisting}
				Which results in:
				\begin{lstlisting}
cov_u =   0.6263    0.1880
  		  0.1880    0.0626
cov(y(:,3)) = 0.6339
cov(y(:,4)) = 0.0627
				\end{lstlisting}
				Therefore the analytical and simulate covariances match up. 
				
				%Part 2.c.i.B
				\item We can simulate the time-domain response of $u$ using the following piece of code:
				\begin{lstlisting}
subplot(2,1,1); 
plot(tspan(1:i),y(1:i,3));
title('Fuel Index $\gamma$','Interpreter','latex');
xlabel('Time [s]','Interpreter','latex');

subplot(2,1,2); 
plot(tspan(1:i),y(1:i,4));
title('Swashplate Angle $\alpha$','Interpreter','latex');
xlabel('Time [s]','Interpreter','latex');
				\end{lstlisting}
				Which results in the following figure:
				\begin{figure}[H]
				\centering
				\includegraphics[scale = 0.8, trim = 1.5in 3.25in 1.5in 3.25in, clip]{Q2cib}
				\end{figure}				
		The control design meets the stated closed loop specifications for the control input because $\gamma \leq 10$ and $\alpha \leq 2.5$. We can check this analytically by using: 
		\begin{lstlisting}
%% Check the standard deviation
[std_u,~] = cov2corr(cov_u);
max_u_95 = 2*std_u;
		\end{lstlisting}
		Which tells us that in two standard deviations (95\% of the time), $u_1 = \gamma$ won't exceed 1.5828, and $u_2 = \alpha$ won't exceed 0.5003. By checking for two standard deviations, we make sure that a good majority of the time, we won't be violating our closed-loop specifications. 
			\end{enumerate}
				
		% Part 2cii
		\item $\Phi_{ew}$ is the closed loop transfer function from $w$ to $e$
			\begin{enumerate}
				% Part 2.c.ii.A.
				\item The appropriate \mcode{sysic} code is shown below: 
%				\begin{figure}[H]
%				\centering
%				\includegraphics[scale = 0.5, trim = 0in 0in 0in 0in, clip]{Q2ciiapdf}
%				\end{figure}
%				This translates into the following \mcode{sysic} model. 
				\begin{lstlisting}
% Transfer function from w to u (sesic)
systemnames = 'K G Wr';
inputvar = '[w{1}]';
outputvar = '[Wr - G]';
input_to_K = '[Wr-G]';
input_to_G = '[K]';
input_to_Wr = '[w]';
sysoutname = 'Phi_ew'; cleanupsysic = 'yes'; sysic;
				\end{lstlisting}
				Using $\Phi_{ew}$, we can analytically solve for the covariance of $e$. 
				\begin{lstlisting}
% Analytically calculating covariance of u
cov_e = Phi_ew.c*lyap(Phi_ew.a,Phi_ew.b*(Phi_ew.b'))*Phi_ew.c'

% Calculating the covariance of the simulated u	
cov(y(:,1)) % cov(e1)
cov(y(:,2)) % cov(e2)
				\end{lstlisting}
				Which results in:
				\begin{lstlisting}
cov_e =  1.0e-04 *
    0.0000    0.0001
    0.0001    0.1156
cov(y(:,1)) = 4.7798e-07
cov(y(:,2)) = 4.0958e-06
				\end{lstlisting}
				Therefore the analytical and simulate covariances match up. 
				
				%Part 2.c.ii.B
				\item We can simulate the time-domain response of the system using the following piece of code:
				\begin{lstlisting}
%% Simulate and plot system response
i = 100;
figure(1); 
err_er = re-y(:,1); % ye (re): engine true, y: error, re-y = v: 
subplot(2,1,1); hold on;
plot(tspan(1:i),err_er(1:i), 'r', tspan(1:i),re(1:i),'b:')
plot_title = title('Engine Speed $\omega _e$','Interpreter','latex');
y_axis_label = ylabel('Frequency [rad/s]','Interpreter','latex');
x_axis_label = xlabel('Time [s]','Interpreter','latex');
legend('$v_e$','$r_e$')
hold off;

err_mr = rm-y(:,2);
subplot(2,1,2); hold on;
plot(tspan(1:i),err_mr(1:i),'r', tspan(1:i),rm(1:i),'b:')
plot_title = title('Vehicle Speed $\omega_m$','Interpreter','latex');
y_axis_label = ylabel('Frequency [rad/s]','Interpreter','latex'); 
x_axis_label = xlabel('Time [s]','Interpreter','latex');
legend('$v_m$','$r_m$')
hold off;
				\end{lstlisting}
				Which results in the following figure, which demonstrates that the tracking error is very small.
				\begin{figure}[H]
				\centering
				\includegraphics[scale = 0.8, trim = 1.5in 3.25in 1.5in 3.25in, clip]{Q2ciib}
				\end{figure}				

			% Standard deviation	
			To further verify that the tracking error meets specifications, we can plot the tracking error itself, which results in the following figure:
				\begin{figure}[H]
				\centering
				\includegraphics[scale = 0.8, trim = 1.5in 3.25in 1.5in 3.25in, clip]{Q2cerror}
				\end{figure}
			As in the previous part (2.c.i.B), we check the closed-loop specifications for two standard deviations. This results in the fact that 95\% of the time, the error in $\gamma$ won't exceed 0, and the error in $\alpha$ won't exceed 0.0068. This satisfies our closed loop specifications of 0.1 and 0.2. 
%			\begin{lstlisting}
%%% Check the standard deviation
%[std_e,~] = cov2corr(cov_e);
%max_e_95 = 2*std_e
%		 = 0.0000   0.0068
%			\end{lstlisting}
		\end{enumerate}
		\end{enumerate}
	% Part 2d
	\item In order to redesign the controller for sensor noise, I'd edit my $W_n$ block to be something other than identity, giving a frequency weighting to the sensor noise, as well as designating it as regulated signal $z_3$ in my \mcode{sysic} model. Passing this through the \mcode{h2syn} function would create a controller which will reduce sensor noise.  
	\end{enumerate}

%% Question 3
\item \textbf{Closed Loop Sensitivity Shaping}
	Following is a block diagram of the general control problem with frequency weightings, where $W_1(s) = W_e(s)$, $W_2(s) = W_u(s)$, and $W_3(s) = W_n(s)$. 
	\begin{figure}[H]
	\centering
	\includegraphics[scale = 0.5, trim = 0in 0in 0in 0in, clip]{Q3di}
	%\caption{System Block Diagram}
	\end{figure}
	\begin{enumerate}
	% Part 3.a
	\item Using the problem statement's specifications of a tracking error with less than 5\% relative error, and a noise rejection with less than 10\% relative error, we can plot the closed loop specifications as follows, where the red box is the tracking error specification and the blue box is the noise rejection specification.
			\begin{figure}[H]
			\centering
			\includegraphics[scale = 0.8, trim = 1.5in 3.25in 1.5in 3.25in, clip]{Q3a}
			\end{figure}
	
	% Part 3.b
	\item In order to design frequency weightings $W_1(s)$ and $W_3(s)$ for the mixed sensitivity specification,
		\begin{equation*}
		\left\|\begin{bmatrix}
		W_1(s)S(s)\\ W_3(s)T(s)
		\end{bmatrix}\right\|_{H_\infty} \leq \gamma
		\end{equation*}
	we create transfer functions which yield plots of $1/W_1(s)$ which runs under the tracking error (red) box and of $1/W_3(s)$ which runs under the noise rejection (blue) box. Doing this will lead to the following plot:
		\begin{figure}[H]
		\centering
		\includegraphics[scale = 0.8, trim = 1.5in 3.25in 1.5in 3.25in, clip]{Q3b}
		\end{figure}
	The transfer functions used in the plot above are:
		\begin{equation*}
		\begin{split}
			W_1(s) &= 50\left(\frac{s}{10^{-1}}+1\right)^{-1}\left(\frac{s}{10^0}+1\right)^{-2}\\
			W_3(s) &= 0.02\left(\frac{s}{1.75}+1\right)^2\left(\frac{s}{10^2}+1\right)^{-2}
		\end{split}
		\end{equation*}
	
	% Part 3.c
	\item Our goal in tuning proper frequency weightings is to have them both stay out of their respective specification boxes and help synthesize a controller which will be stable. We can check this by evaluating the $H_\infty$ norm of the closed-loop system. If on the first attempt of tuning the frequency weightings, we can plot the singular values of the three curves (the closed-loop mixed function, $W_1(s)S(s)$, and $W_3(s)T(s)$). This plot is depicted below:
		\begin{figure}[H]
		\centering
		\includegraphics[scale = 0.8, trim = 1.5in 3.25in 1.5in 3.25in, clip]{Q3ci}
		\end{figure}	
	In the case that the closed loop system isn't stable (the CL system is stable when the magnitude of the singular values are less than 1), we can use the above figure to tell us where the mixed function is running above 1, and which frequency weighting is pushing it over the edge and adjust that function accordingly.  
	On the other hand, we can use properly tuned frequency weightings to synthesize a stable controller in MATLAB with the \mcode{mixsyn} function: \mcode{[K, CL, GAM, INFO] = mixsyn(G, W1, W2, W3)}, where we designate \mcode{W2 = 0}. This will result in the following plot and a norm (\mcode{GAM}) of less than 1 (in our case, 0.6899):
		\begin{figure}[H]
		\centering
		\includegraphics[scale = 0.8, trim = 1.5in 3.25in 1.5in 3.25in, clip]{Q3cii}
		\end{figure}	
	As we can see, in each case the closed loop sensitivity and complementary sensitivity functions run underneath their respective frequency weighting functions. 
	  
	% Part 3.d.
	\item In order to also achieve zero-steady-state error $e(t)$ for a step input in $r(t)$, we can add an integrator to the system. 
		
		\begin{enumerate}
		% Part 3.d.i.
		\item We first draw the block diagram of the frequency weighted closed loop system augmented with an integrator to yield integral control:
			\begin{figure}[H]
			\centering
			\includegraphics[scale = 0.5, trim = 0in 0in 0in 0in, clip]{Q3dii}
			%\caption{System Block Diagram}
			\end{figure}
		
		% Part 3.d.ii.
		\item The general, unweighted control problem can be formulated in \mcode{sysic} as follows:
			\begin{lstlisting}
%% General System (Unweighted)
systemnames = 'G';
inputvar = '[w{1};u{1}]';
outputvar = '[w(1)-G;u;G;w(1)-G]';
input_to_G = '[u]';
sysoutname = 'Pgen_uw'; cleanupsysic = 'yes'; sysic;
			\end{lstlisting}
		With the frequency weightings from part c, the general, weighted control problem can be formulated as follow:
			\begin{lstlisting}
%% General System with Weightings
systemnames = 'G W1 W2 W3';
inputvar = '[w{1};u{1}]';
% outputvar = '[W1;W2;W3;u;w(1)-G]'; %[z1;z2;z3;u;y]
outputvar = '[W1;W2;W3;w(1)-G]';
input_to_G = '[u]';
input_to_W1 = '[w(1)-G]';%w(1) = r
input_to_W2 = '[u]';
input_to_W3 = '[G]';
sysoutname = 'Pgen'; cleanupsysic = 'yes'; sysic;
			\end{lstlisting}
		
		% Part 3.d.iii.
		\item We can synthesize a controller $K(s)$ using the $H_\infty$ synthesis algorithm in MATLAB as follows:
		\begin{lstlisting}
%% Augmented System with Weightings
int = 1/s; %integrator
systemnames = 'G W1 W2 W3 int';
inputvar = '[w{1};u{1}]';
outputvar = '[W1; W2; W3; int]';
input_to_G = '[u]';
input_to_W1 = '[w(1)-G]';
input_to_W2 = '[u]';
input_to_W3 = '[G]';
input_to_int = '[w(1)-G]';
sysoutname = 'Paug'; cleanupsysic = 'yes'; sysic;

[Ka,CLa,GAMa, INFOa] = hinfsyn(ss(Paug), nmeas,ncon,'method','lmi');
sys_aug = lft(Paug,Ka); % augmented, weighted
sys_aug_uw = lft(Paug_uw,Ka); % augmented, unweighted
		\end{lstlisting}
We can plot the closed loop system response to a step input by using the MATLAB function \mcode{lsim} on the augmented, unweighted closed loop system. We take the first output, $E(s)$, and subtract it from the reference step input in order to plot the measured signal against the reference input. This plot is shown below, and since the measured signal matches up with the reference input as time goes on, we know that our error goes to zero at steady-state, thereby satisfying our specification. 
		\begin{figure}[H]
		\centering
		\includegraphics[scale = 0.8, trim = 1.5in 3.25in 1.5in 3.25in, clip]{Q3dsystemresponse}
		\end{figure}
		\end{enumerate}
	% Part 3.e
	\item The presence of an unstable (right-hand) pole at $s = p$ requires that the bandwidth frequency, $\omega_c$, of the sensitivity $S(s)$ is greater than $2p$. If this is not true, the input may saturate when there are disturbances and the system will be incapable of stabilizing (Sec. 5.13, Rule 8). In this problem, we have a right-hand pole at $s = p = 5$, which means that our bandwidth frequency must be slower than 10 rad/s. By looking at the bode plot of our closed-loop sensitivity, we get the following figure.
		\begin{figure}[H]
		\centering
		\includegraphics[scale = 0.8, trim = 1.5in 3.25in 1.5in 3.25in, clip]{Q3e}
		\end{figure}
	We can visually interpret that the bandwidth frequency (where the magnitude plot of the sensitivity curve crosses -3dB from below) is around 1.5 rad/s currently. Though this does not satisfy Rule 8, we can still get a stable closed loop system. However, this slower bandwidth may explain the large oscillations that occur in the system response. It's possible that by modifying the sensitivity bandwidth, we may result in an even better, more stable system which will hold for larger inputs. 
	\end{enumerate}

% Question 4
\item \textbf{Nominal Performance, Robust Stability, and Robust Performance}
\begin{equation*}
	G = \begin{cases}
			\dot{x}(t) = Ax(t) + Bu(t)\\
			y(t) = Cx(t) + Du(t)
	\end{cases}
\end{equation*}
where
\begin{equation*}
A = \begin{bmatrix}
	0 & 0 & 1 & 0\\
	1.5 & -1.5 & 0 & 0.018/\pi\\
	-12 & 12 & -0.8 & -0.108/\pi\\
	-48.84\pi/180 & 10.64\pi/180 & 0 & -0.014
\end{bmatrix}, \textrm{ } B= \begin{bmatrix}
	0 & 0\\
	0.16 & 0.6\\
	-19 & -2.5 \\
	-0.66\pi/180 & -0.5\pi/180
\end{bmatrix}
\end{equation*}
\begin{equation*} 
C = \begin{bmatrix}
	1 & 0 & 0 & 0\\
	0 & 1 & 0 & 0
\end{bmatrix}, \textrm{ } D = \begin{bmatrix}
	0
\end{bmatrix}
\end{equation*}
	\begin{enumerate}
	% Part 4.a.
	\item To design a controller for the nominal specifications of steady-state tracking error for step input commands, we must augment the system with an integrator. In order to meet the specification of 2\% tracking error for sinusoidal angle commands, we must design weighting frequencies. 
	
	To start, we work with the weighting frequency. Since we're only looking to deal with tracking error, we only need $W_e(s) = W_1(s)$, not $W_2(s)$ or $W_3(s)$. We can try:
	\begin{equation*}
	W_1 = 100\left(\frac{s}{10^{-1}}+1\right)^{-1}\left(\frac{s}{10^0}+1\right)^{-2}
	\end{equation*} Plotting this weighting frequency and the 2\% tracking error box gives us the following plot. 
		\begin{figure}[H]
		\centering
		\includegraphics[scale = 0.8, trim = 1.5in 3.25in 1.5in 3.5in, clip]{Q4ai}
		\end{figure}
		
	Using \mcode{sysic}, we can augment the system with an integrator to satisfy the zero steady-state tracking error specification. We specify $W_2(s)$ and $W_3(s)$ to be zero and run the following commands:
	\begin{lstlisting}
%% Augmented System (Weighted)
systemnames = 'G W1 W2 W3 int';
inputvar = '[w{2};u{2}]';
outputvar = '[W1; W2; W3; int]';
input_to_G = '[u]';
input_to_W1 = '[w-G]';
input_to_W2 = '[u]';
input_to_W3 = '[G]';
input_to_int = '[w-G]';
sysoutname = 'Paug'; cleanupsysic = 'yes'; sysic;

%% Design a Controller using hinfsyn
nmeas = 2; ncon = 2;
[Ka,CLa,GAMa, INFOa] = hinfsyn(ss(Paug), nmeas,ncon,'method','lmi'); %
sys_aug = lft(Paug, Ka);
	\end{lstlisting}
	We can demonstrate that this controller meets the nominal performance specifications by simulating the system for step input and sinusoidal flight angle commands, which result in the following plots. 
		\begin{figure}[H]
		\centering
		\includegraphics[scale = 0.8, trim = 1.5in 3.25in 1.5in 3.25in, clip]{Q4aiii}
		\end{figure}	
	
		\begin{figure}[H]
		\centering
		\includegraphics[scale = 0.8, trim = 1.5in 3.25in 1.5in 3.25in, clip]{Q4aiv}
		\end{figure}
		
	In addition, we can generate frequency response plots to demonstrate that the performance specifications are met: 
		\begin{figure}[H]
		\centering
		\includegraphics[scale = 0.8, trim = 1.5in 3.25in 1.5in 3.25in, clip]{Q4aii}
		\end{figure}
	Since the sensitivity curve falls below the inverse weighting function curve, we know that our system is robustly stable. 			
	
	% Part 4.b.
	\item 
		\begin{figure}[H]
		\centering
		\includegraphics[scale = 0.8, trim = 1.5in 3.25in 1.5in 3.25in, clip]{Q4bi}
		\end{figure}
	By looking at this plot, we can tell that the uncertain system is not robustly stable because the maximum singular value is 5.93 dB, which has a magnitude of 1.9792. To be robustly stable, the maximum singular value must have a magnitude of less than 1. Therefore, we can adjust $W_1(s)$ to make the uncertain system stable. 
		\begin{equation*}
		W_1(s) = 100\left(\frac{s}{10^{-1}}\right)^{-3}
		\end{equation*}
		\begin{figure}[H]
		\centering
		\includegraphics[scale = 0.8, trim = 1.5in 3.25in 1.5in 3.5in, clip]{Q4bistable}
		\end{figure}
	The maximum singular value is now 0.71.
	
	% Part 4.b.ii
	\item Constructing a model of the uncertain closed-loop system
	\begin{enumerate}
		% Part 4.b.ii.A
		\item The plot of the frequency domain results of the robust stability analysis appears as:
		\begin{figure}[H]
		\centering
		\includegraphics[scale = 0.8, trim = 1.5in 3.25in 1.5in 3.5in, clip]{Q4biiA}
		\end{figure} 
		%Part 4.b.ii.B
		\item This matches up with the previous plot, indicating that the uncertain closed-loop system is robustly stable.
		% talk more about mu-analysis, and what the inverse means.  
	\end{enumerate}
	
	% Part 4.c.
	\item \textbf{Robust Performance}
		\begin{enumerate}
		% Part 4.c.i.
		\item Combining nominal performance and robust stability into a single robust stability problem. We start by reframing the robust performance analysis with the uncertain system: 
			\begin{equation*}
			LFT(\tilde{\Delta},\tilde{M}) = LFT(\Delta_p,W_eT_{er}) = LFT(\Delta_p, W_eLFT(\Delta_u,M))
			\end{equation*}
			and the augmented uncertainty:
			\begin{equation*}
			\tilde{\Delta} = \begin{bmatrix}
			\Delta_p & 0\\
			0 & \Delta_u
			\end{bmatrix}, \|\tilde{\Delta}\|_{H_\infty} \leq 1
			\end{equation*}
		The block diagram for this is as follows: 
			\begin{figure}[H]
			\centering
			\includegraphics[scale = 0.5, trim = 0in 0in 0in 0in, clip]{Q4ci_general}
			%\caption{System Block Diagram}
			\end{figure}		
		which more specifically, is:
			\begin{figure}[H]
			\centering
			\includegraphics[scale = 0.5, trim = 0in 0in 0in 0in, clip]{Q4cideltap}
			%\caption{System Block Diagram}
			\end{figure}				
		and is modeled in sysic as:
%		\begin{lstlisting}
%		
%		\end{lstlisting}
		\begin{figure}[H]
		\centering
		\includegraphics[scale = 0.8, trim = 1.5in 3.25in 1.5in 3.5in, clip]{Q4ci_tilde}
		\end{figure}
Therefore, the system is robustly stable. 
		% Part 4.c.ii
		% Part 4.c.iii
		\end{enumerate}
	\end{enumerate}
\end{enumerate}
\end{document}