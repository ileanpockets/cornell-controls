% Eileen J. Kim (ejk93)
% MAE 6780, HW # 3,  Problem # 2
% Start: April 23, 2014

%% Problem Set-Up
% Clearing
close all
clear all
clc

A = [-0.8409 -9.699 0;
    0.1803 0.8461 -0.5271;
    -2.03 2440 -28.01];

B = [29.15 -22.11;
    0.01547 6.976;
    0.02799 -11.54];

C = [1 0 0;
    0 0 1];

D = zeros(2,2);

% Constraints
gamma_max = 10; % max u1
alpha_max = 2.5; % max u2
eT_e = 0.1; %tracking re
eT_m = 0.2; %tracking rm
%% PART A: 
% A.i.)
G = ss(A, B, C, D);

% Frequency Weightings
s = tf('s');
We = 1/(s + 0.5); % given engine speed weighting
km = 10^(1.8); 
Wm = zpk(km*(s/(1e-1)+1)^(-2)); %from problem 1 
Wp = [1/eT_e 0;
      0 1/eT_m]; %penalize tracking error, normalize the value
% Wu = 10^(1.5)*[1/gamma_max 0;
%       0 1/alpha_max]; % penalize control effort
  Wu = [1/gamma_max 0;
      0 1/alpha_max]; 
Wr = [Wm*We;Wm];
Wn = .001*eye(2); % required to add noise to the system

%% OPEN LOOP SYSTEM P
% sysic
systemnames = 'G Wp Wu Wr Wn';
inputvar = '[w{3};u{2}]';
outputvar = '[Wp; Wu; Wr-G-Wn]'; %[z1; z2; y]
input_to_G = '[u]';
input_to_Wp = '[Wr-G]';
input_to_Wu = '[u]';
input_to_Wr = '[w(1)]';
input_to_Wn = '[w(2:3)]';
sysoutname = 'P'; cleanupsysic = 'yes'; sysic;

%% Synthesize an H2 Controller
nmeas = 2; ncon = 2;
[K, CL, GAM, INFO] = h2syn(P,nmeas,ncon);
gam1 = norm(CL, inf);

%% Checking Errors
CL2sys = lft(P,K);
dt = 0.5;
n = 5e5;
tspan = 0:dt:n*dt;
n = randn(2,length(tspan))/sqrt(dt);    %engine and vehicle noise
w = randn(1,length(tspan))/sqrt(dt);    %engine and vehicle reference input
[y,t] = lsim(CL2sys,[w;n],tspan);       %entire system - y: [we, wm, ne, nm]
[re,t] = lsim((We*Wm),w,tspan);         % engine reference, re
[rm,t] = lsim(Wm,w,tspan);              %vehicle reference, rm
y = y(:,(1:4))/mdiag(Wp,Wu);% divide by the weighting functions to get the actual values

%% Plot Tracking Error
i = 100;
figure(1); 
err_er = re-y(:,1); % true = ref - error 
subplot(2,1,1); hold on;
plot(tspan(1:i),err_er(1:i), 'r', tspan(1:i),re(1:i),'b:')
title('Engine Speed $\omega _e$','Interpreter','latex');
ylabel('Frequency [rad/s]','Interpreter','latex');
xlabel('Time [s]','Interpreter','latex');
legend('$v_e$','$r_e$')
hold off;

err_mr = rm-y(:,2);
subplot(2,1,2); hold on;
plot(tspan(1:i),err_mr(1:i),'r', tspan(1:i),rm(1:i),'b:')
title('Vehicle Speed $\omega_m$','Interpreter','latex');
ylabel('Frequency [rad/s]','Interpreter','latex'); 
xlabel('Time [s]','Interpreter','latex');
legend('$v_m$','$r_m$')
hold off;


%% Verifying CL System

% Transfer function from r to u
sys_ur = feedback(K,G);

% Transfer function from w to u (sysic)
systemnames = 'sys_ur Wr';
inputvar = '[w{1}]';
outputvar = '[sys_ur]'; %[z1; z2; y]
input_to_sys_ur = '[Wr]';
input_to_Wr = '[w]';
sysoutname = 'Phi_uw'; cleanupsysic = 'yes'; sysic;

% Analytically calculating covariance of u
cov_u = Phi_uw.c*lyap(Phi_uw.a,Phi_uw.b*(Phi_uw.b'))*Phi_uw.c';

% Verifying our analytical cov(u)
covu1 = cov(y(:,3)); % u1
covu2 = cov(y(:,4)); % u2

% Printing to workspace
fprintf('The analytical covariance, calculated using the transfer function, of u1 is %.3f. The actual covariance of u1 is %.3f.\n',cov_u(1,1), covu1);
fprintf('The analytical covariance, calculated using the transfer function, of u2 is %.3f. The actual covariance of u1 is %.3f.',cov_u(2,2), covu2);

%% Simulate and plot the time response of u
% Plot Control Input
figure(2);
subplot(2,1,1); hold on;
plot(tspan(1:i),y(1:i,3));
title('Fuel Index $\gamma$','Interpreter','latex');
xlabel('Time [s]','Interpreter','latex');

subplot(2,1,2); 
plot(tspan(1:i),y(1:i,4));
title('Swashplate Angle $\alpha$','Interpreter','latex');
xlabel('Time [s]','Interpreter','latex');


%% Check the standard deviation
[std_u,~] = cov2corr(cov_u);
max_u_95 = 2*std_u;

%% Verifying CL system
% Covariance of e

systemnames = 'K G Wr';
inputvar = '[w{1}]';
outputvar = '[Wr - G]'; %[z1; z2; y]
input_to_K = '[Wr-G]';
input_to_G = '[K]';
input_to_Wr = '[w]';
sysoutname = 'Phi_ew'; cleanupsysic = 'yes'; sysic;

% Analytically calculating covariance of e
cov_e = Phi_ew.c*lyap(Phi_ew.a,Phi_ew.b*(Phi_ew.b'))*Phi_ew.c';

% Verifying our analytical cov(e)
cove1 = cov(y(:,1)); % cov(e1)
cove2 = cov(y(:,2)); % cov(e2)

% Printing to workspace
fprintf('The analytical covariance, calculated using the transfer function, of e1 is %.3f. The actual covariance of u1 is %.3f.\n',cov_e(1,1), cove1);
fprintf('The analytical covariance, calculated using the transfer function, of e2 is %.3f. The actual covariance of u1 is %.3f.',cov_e(2,2), cove2);

%% Check the standard deviation
[std_e,~] = cov2corr(cov_e);
max_e_95 = 2*std_e;

%% Checking Error
figure(3); clf; 
subplot(2,1,1); 
percent_error_e = (y(:,1)./re);
plot(tspan(1:100),percent_error_e(1:100));
title('Engine Speed Error', 'Interpreter', 'latex'); 
xlabel('Time [s]', 'Interpreter', 'latex')
ylabel('Percent [\%]', 'Interpreter', 'latex');

subplot(2,1,2); 
percent_error_m = (y(:,2)./rm);
plot(tspan(1:100),percent_error_m(1:100));
title('Vehicle Speed Error', 'Interpreter', 'latex');
xlabel('Time[s]', 'Interpreter', 'latex');
ylabel('Percent [\%]', 'Interpreter', 'latex');
