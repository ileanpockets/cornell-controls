function problem()
close all
clear all
clc

%% Define discrete-time system of the mag-lev system
A = [ 0   1   0
     980  0  -2.8
      0   0  -100 ];
B = [ 0
      0
      100 ];
C = [ 1 0 0 ];
Gsys = ss(A,B,C,0);
Ts = 0.01; % sampling time
Tp = 0.2; % [s] choose prediction horizion length
Gdsys = c2d(Gsys,Ts);
clear Gsys A B C

% SIMULATION PARAMETERS
x0 = [0.01;0;0]; % simulation initial conditions
Tf=0.6; % [s] final time of simulation

%% Define Cost Function
% $J(x,u)=x_N^*P_Nx_N+\sum_{k=t}^{t+N-1} x_k^*Qx_k+u_k^*Ru_k$
[n,m] = size(Gdsys.B);
Q = eye(3); % state penalty
R = eye(m)*1e0; % input penalty
PN = Q; % terminal penalty
%Tp = 0.1; % [s] choose prediction horizion length
Np = ceil(Tp/Gdsys.ts); % time steps in prediction horizon

%% Part a: Batch solution for MPC controller
[K,P] = batch(Gdsys,PN,Q,R,Np); %calculate control law and value function
Kx = @(x0)[eye(m,m*Np)*K*x0]; % define control law function handle
Vx = @(x0)[x0'*P*x0]; % define value-function function handle

% SIMULATE CLOSED LOOP MPC
Nf = ceil(Tf/Gdsys.ts); % # of simulaiton time steps
[X,U,J,T]=simulate(Gdsys,Kx,Vx,Nf,x0); % simulate closed loop system
T = T*Gdsys.ts; % convert time steps to seconds
figure; 

% set(gcf,'NextPlot','add');
% axes;
% h = title('MyTitle');
% set(gca,'Visible','off');
% set(h,'Visible','on');
plotresults(X,U,J,T);
title(['T_s=',num2str(Gdsys.Ts),', T_p=' num2str(Np*Gdsys.Ts)]);
% str1 = sprintf('T_s = %f, T_p = %f', Gdsys.Ts, Np*Gdsys.Ts);
% title(str1)

%% Part b: Recursive solution for MPC controller
P_kplus1 = PN;
for k = (Np-1):-1:0; % recursively work from last stage to first stage
	[K_k,P_k] = onestep(Gdsys,P_kplus1,Q,R); % initialize control law
	P_kplus1 = P_k; % new terminal cost (cost to go)
end
K0 = -K_k; % control law at stage k=0: u0 = K0*x0
P0 = P_k; % value function at stage k=0: V0(x0) = x0'*P0*x0
Kx = @(x0)[K0*x0]; % define control law function handle
Vx = @(x0)[x0'*P0*x0]; % define value-function function handle

% SIMULATE CLOSED LOOP MPC
Nf = ceil(Tf/Gdsys.ts); % # of simulaiton time steps
[X,U,J,T]=simulate(Gdsys,Kx,Vx,Nf,x0); % simulate closed loop system
T = T*Gdsys.ts; % convert time steps to seconds;
figure; plotresults(X,U,J,T);
title(['T_s=',num2str(Gdsys.ts),', T_p=' num2str(Np*Gdsys.ts)]);

%% SUBFUNCTIONS
function [X_MPC,U_MPC,V_MPC,T]=simulate(sys,Kx,Vx,N,x0)
%% Simulate Model Predictive Controller
% sys: discrete time system
% K: state feedback gain
% P: value function matrix
% Tf: simulation time
% x0: initial condition
A = sys.a; B = sys.b;
[n,m] = size(B);
xk=x0; % initialize simulation variables
X_MPC=ones(n,N); U_MPC=ones(m,N); V_MPC=ones(1,N); 
for k=0:N-1
    % CALCULATE OPEN LOOP SOLUTION
	uk = Kx(xk); % calculate control law
	xkplus1 = A*xk+B*uk; % SIMULATE DIFFERENCE EQUATION
	X_MPC(:,k+1) = xk; U_MPC(:,k+1) = uk; V_MPC(k+1) = Vx(xk);
	xk=xkplus1; % prepare for next iteration
end
k=k+1;
X_MPC(:,k+1) = xk; U_MPC(:,k+1) = uk; V_MPC(k+1) = Vx(xk);
T=0:N;
return;

function plotresults(X,U,J,T)
% PLOT RECEDING HORIZON CONTROL
subplot(2,2,1); stairs(T,X(1,:)); ylabel('\delta z [m]');
subplot(2,2,2); stairs(T,X(2,:)); ylabel('\delta i [A]');
subplot(2,2,3); stairs(T,U); ylabel('\delta v [V]');
xlabel('time [s]');
subplot(2,2,4); stairs(T,J); ylabel('value function');
xlabel('time [s]');
return;

function [K,P] = batch(sys,PN,Q,R,N)
%% Batch optimization of cost function
% $J(x,u)$ over $(x,u)$ where
% $J(x,u)=x_N^*P_Nx_N+\sum_{k=0}^{N-1} x_k^*Qx_k+u_k^*Ru_k$
% sys: discrete system dynamics
% K: statefeedback gain
% P: value function matrix $V(x)=x_0^*Px_0=\min_{(x,u)} J(x,u)$
A = sys.a; B = sys.b;
[n,m] = size(B);

% Initializing parameters for the for loop
scriptC = [B]; % Controllability Matrix (scriptC)
S_u = [zeros(n,m); scriptC];
S_x = [eye(n); A];

for k= 1:N-1
    scriptC = [A*scriptC, B]; 
    S_u = [[S_u, zeros((k+1)*n,m)]; scriptC];
    S_x = [eye(n); S_x*A];
end

Qbar = [[kron(eye(N),Q), zeros(size(Q,1)*N,size(Q,2))];[zeros(size(Q,1),size(Q,2)*(N)),PN]];
% Qbar = kron(eye(N+1),PN);
Rbar = kron(eye(N),R);
F = (S_x)'*Qbar*S_u;
H = Rbar + (S_u)'*Qbar*S_u;
Y = (S_x)'*Qbar*S_x;

K = -inv(H)*F'; % state feedback gain
P = Y - F*inv(H)*F'; % V(x_0)=x_0^*Px_0 value function
return;

function [K_k,P_k] = onestep(sys,P_kplus1,Q,R)
% Optimization of one step cost function
% $J(x,u)$ over $(u_k)$ where
% $J(x,u)= x_k^*Qx_k+u_k^*Ru_k+x_{k+1}^*P_{k+1}x_{k+1}$
		
% sys: discrete-time open-loop state space system
% P_kplus1: value function @ stage k+1: $V_{k+1}(x_{k+1})$
		
% K_k: statefeedback gain @ stage k: $u_k=K_kx_k$
% P_k: value function @ stage k: $V_k(x_k)$
A = sys.a; B = sys.b;
[n,m] = size(B);

K_k = inv(R+B'*P_kplus1*B)*(B'*P_kplus1*A); % ---EDIT THIS LINE---- % derive state feedback gain
P_k = A'*P_kplus1*A + Q-A'*P_kplus1*B*inv(R+B'*P_kplus1*B)*B'*P_kplus1*A; % ---EDIT THIS LINE---- % V(x_k)=x_k^*Px_k

return;