function MAE6780HW2Q4
% MAE 6780 HW 2, Q4
% Eileen J. Kim (ejk93)
% March 26, 2014

% Clearing 
close all
clear all
clc

% Set-up
% Original Matrices
A = [0 1 0;
    980 0 -2.8;
    0 0 -100];
B = [0 0 100]';
Cz = [10 1 0; 
    0 0 0];
Cy = [1 0 0];
Dzu = [0 1]';

x0 = [-0.015 0 0]';

% Augmented Matrices
A_til = [A, zeros(3,1); -Cy 0]; %4x4 
B_til = [B; 0]; %4x1
Br_til = [zeros(3,1); 1]; %4x1
[m_b, n_b] = size(B_til); %4x1
x0_til = [x0; 0]; %4x1


% Continuous-time state feedback LMI
% u(t) = -K1*x(t) - K2*z(t)
cvx_begin sdp quiet
    variable Y(m_b,m_b) symmetric %4x4
        variable J(n_b,m_b) %1x4
        variable c
    (A_til*Y - B_til*J) + (A_til*Y - B_til*J)' <= -c*eye(size(A_til));
    [Y J'; J eye(n_b)] >= 0;
    [100 x0_til'; x0_til Y] >= 0;
    Y > 0;
    minimize(-c)
cvx_end

K_til = J/Y
K_til(4)
disp(eig(A_til - B_til*K_til)); % display closed loop poles

% Plot figure
figure(1); clf;
tf = 1;
initial(ss(A_til-B_til*K_til, [], [Cy 0], []), x0_til, tf); ylabel('response')
u = -K_til*x0_til