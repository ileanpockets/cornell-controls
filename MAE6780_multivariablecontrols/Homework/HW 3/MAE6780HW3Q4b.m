% Eileen J. Kim (ejk93)
% MAE 6780 HW # 3, Q #4b

% Clearing
close all
clear all
clc

%% System Set-up
A = [0 0 1 0;
    1.5 -1.5 0 0.018/pi;
    -12 12 -0.8 -0.108/pi;
    -48.84*pi/180 10.64*pi/180 0 -.014];
B = [0 0;
    0.16 0.6;
    -19 -2.5;
    -0.66*pi/180 -.5*pi/180];

C = [1 0 0 0;
    0 1 0 0];

D = zeros(2,2);

G = ss(A,B,C,D);
s = tf('s');

%% Robust Stability
W = (10*s/(s+200))*eye(2);
k1 = 100;
% w1 = k1*(s/(1e-1)+1)^(-1)*(s/1e0+1)^(-2); %used a W1 similar to Q3, but with different gain
% w1 = ((10^2))*((1)/(((s/(10^-1))+1)^3)); 
w1 = k1*(s/(1e-1)+1)^(-3);
W1 = ss(w1*eye(2));
W2 = ss(0*eye(2));
W3 = ss(0*eye(2));
%% zero steady-state tracking error for step input commands: (integrator)

int = ss((1/s)*eye(2));

%% Augmented System (Weighted)
systemnames = 'G W1 W2 W3 int';
inputvar = '[w{2};u{2}]';
outputvar = '[W1; W2; W3; int]';
input_to_G = '[u]';
input_to_W1 = '[w-G]';
input_to_W2 = '[u]';
input_to_W3 = '[G]';
input_to_int = '[w-G]';
sysoutname = 'Paug'; cleanupsysic = 'yes'; sysic;

%% Augmented System (Unweighted)
systemnames = 'G int';
inputvar = '[w{2};u{2}]';
outputvar = '[w-G; u; G;int]'; 
input_to_G = '[u]';
input_to_int = '[w-G]';
sysoutname = 'Paug_uw'; cleanupsysic = 'yes'; sysic;

%% Design a Controller using hinfsyn
nmeas = 2; ncon = 2;
[Ka,CLa,GAMa, INFOa] = hinfsyn(ss(Paug), nmeas,ncon,'method','lmi'); % Augmented system

%% Augmented System (Weighted with Uncertainty)
systemnames = 'G W1 W2 W3 W int';
inputvar = '[Delta_u{2}; w{2};u{2}]';
outputvar = '[W; W1; W2; W3; int]';
input_to_G = '[u]';
input_to_W1 = '[w-G-Delta_u]';
input_to_W2 = '[u]';
input_to_W3 = '[G]';
input_to_int = '[w-G-Delta_u]';
input_to_W = '[G]';
sysoutname = 'uncP'; cleanupsysic = 'yes'; sysic;

M = lft(uncP,Ka);
figure(1)
nz_delta = 2; nw_delta = 2;
sigma(M(1:nz_delta,1:nw_delta));
title('Singular Values', 'Interpreter', 'latex')
xlabel('Frequency', 'Interpreter', 'latex')
ylabel('Singular Values', 'Interpreter', 'latex')
% analyze robust stability of closed-loop uncertain system?
% GAMa
%% Part b.ii
Delta_u = ultidyn('Delta_u',[2 2], 'Bound', 1);
T_er = lft(Delta_u,M);
[stabmarg, destabunc,report,info] = robuststab(T_er);

% Plot frequency domain results of robust stability analysis
figure(2)
LinMagopt = bodeoptions;
LinMagopt.PhaseVisible = 'off'; LinMagopt.MagUnits = 'dB';
bodeplot(info.MussvBnds(1,1),info.MussvBnds(1,2),LinMagopt);
xlabel('Frequency [rad/s]', 'Interpreter', 'latex'); 
ylabel('Mu upper/lower bounds', 'Interpreter', 'latex');
title('Mu plot of robust stability margins (inverted scale)', 'Interpreter', 'latex');
% Stability margin is related to the inverse of the Hinf norm

%% Part c: Robust performance
% Part c.i.
%% Augmented System (Weighted with Uncertainty&Robustness)
% systemnames = 'G W1 W';
% inputvar = '[Delta_u{2}; Delta_p{2}; w{2}; u{2}]';
% outputvar = '[W;W1]';
% input_to_G = '[u]';
% input_to_W1 = '[w-Delta_u - Delta_p-G]';
% input_to_W = '[G]';
% sysoutname = 'uncP2'; cleanupsysic = 'yes'; sysic;

% We = W1;
% systemnames = 'G W1 W2 W3 W We';
% inputvar = '[Delta_u{2}; Delta_p{2}; w{2}; u{2}]';
% outputvar = '[W;W1;W2;W3;We]';
% input_to_G = '[u]';
% input_to_W1 = '[w-Delta_u - Delta_p-G]';
% input_to_W = '[G]';
% input_to_W2 = '[u]';
% input_to_W3 = '[G]';
% input_to_W1 = '[w-Delta_u - Delta_p-G]';
% sysoutname = 'uncP2'; cleanupsysic = 'yes'; sysic;

We = W1;
systemnames = 'G W1 W2 W3 W We int';
inputvar = '[Delta_u{2}; Delta_p{2}; w{2}; u{2}]';
outputvar = '[W;W1;W2;W3;We; int]';
input_to_G = '[u]';
input_to_W1 = '[w-Delta_u - Delta_p-G]';
input_to_W = '[G]';
input_to_W2 = '[u]';
input_to_W3 = '[G]';
input_to_W1 = '[w-Delta_u - Delta_p-G]';
input_to_int = '[w-Delta_u-Delta_p-G]';
sysoutname = 'uncP2'; cleanupsysic = 'yes'; sysic;

%% General Unweighted Problem
systemnames = 'G W1 W';
inputvar = '[Delta_u{2}; Delta_p{2}; w{2}; u{2}]';
outputvar = '[W;W1]';
input_to_G = '[u]';
input_to_W1 = '[w-Delta_u - Delta_p-G]';
input_to_W = '[G]';
sysoutname = 'uncP3'; cleanupsysic = 'yes'; sysic;
%%
Delta_p = ultidyn('Delta_p', [2 2], 'Bound', 1);
Delta_tilde = [Delta_p, zeros(2,2);
               zeros(2,2), Delta_u];
[Ka2,CLa2,GAMa2, INFOa2] = hinfsyn(ss(uncP2), nmeas,ncon,'method','lmi');
M_tilde = lft(uncP2,Ka2);
M_tilde2 = lft(uncP2,Ka); % with frequency weightings w1,w2,w3
T = lft(Delta_tilde,M_tilde2);

% M_tilde = w1*eye(6)*T_er;
figure(3)
sigma(M_tilde(1:nz_delta,1:nw_delta))
title('Singular Values', 'Interpreter', 'latex')
xlabel('Frequency', 'Interpreter', 'latex')
ylabel('Singular Values', 'Interpreter', 'latex')

%Part c.ii.
[perfmarg, wcu, report2, info2] = robustperf(T);
figure(4)
LinMagopt = bodeoptions;
LinMagopt.PhaseVisible = 'off'; LinMagopt.MagUnits = 'abs';
bodeplot(info2.MussvBnds(1,1),info2.MussvBnds(1,2),LinMagopt);
xlabel('Frequency', 'Interpreter', 'latex');
ylabel('$\mu$ upper/lower bounds', 'Interpreter', 'latex');
title('$\mu$ plot of robust stability margins (inverted scale)','Interpreter', 'latex');

%% Part c.iii
% Use MATLAB function dksyn to construct a robust controller
% iu = 
% uncP2 = ss2tf(uncP2.a,uncP2.b,uncP2.c,uncP2.d,iu);
% uncP2 = tf(uncP2)
% [K3, clp3, bnd3] = dksyn(uncP2, 2,2);
opt = dkitopt('NumberofAutoIterations',5,'AutoScalingOrder',1);
[K3,clpmu,bnd,dkinfo] = dksyn(uncP3,2,1,opt);

sprintf('D-K iteration controller achieved a bound value of %5.3g',bnd)

figure(5); step(usample(lft(uncP3,K3),20));
title('uncertain closed loop step response');