% Uncertain Parameter Example
% Author: B. Hencey
% Based on demo problem created by P. Gahinet, The MathWorks, Inc.
%  TRACKING LOOP :
%                _______        _______
%   r        e   |     |        |     |-----------+----> azv 
%   ----->O----->|  K  |------->|  G  |           |
%         |-     |     |   u    |_____|----+      |
%         |   +->|_____|                   | q    | 
%         |   |                            |      |
%         |   |____________________________|      |
%         |_______________________________________|
%
%
%  Goal: settling time < 0.5 sec. for the step response of the
%        vertical acceleration azv 
%        
%  Loop-shaping formulation:
%
%         ||  w1 S   ||                          -1
%         ||         ||   <  1 ,       S = (1+GK) 
%         ||  w2 KS  ||oo
%  CHOICE OF SHAPING FILTERS:
%

%%  AFFINE PARAMETER-DEPENDENT MODEL OF THE MISSILE
%
%                      [ -Z_al  1 ]         [ 0 ] 
%           dx/dt  =   [          ]  x   +  [   ]  u
%                      [ -M_al  0 ]         [ 1 ] 
%
%    
%          [ azv ]     [ -1     0 ]
%      y = [     ] =   [          ]  x        (q = pitch rate)
%          [  q  ]     [  0     1 ]
%
%  where the aerodynamical coefficients Z_al and M_al
%  vary in
%
%        0.5 <= Z_al <= 4      0 <= M_al <= 106

%  Specify the range of parameter values (parameter box)

clear all; close all;
Zmin=.5;  Zmax=4;
Mmin=0;   Mmax=106;

% define uncertain parameters
deltaZ=ureal('deltaZ',mean([Zmin Zmax]),'Range',[Zmin Zmax]);
deltaM=ureal('deltaM',mean([Mmin Mmax]),'Range',[Mmin Mmax]);

clear Zmin Zmax Mmin Mmax;
%% Definition of Controller Weightings

%%
% The 0.5 sec settling time specification implies our dominant closed loop
% poles shoud be faster than $Re(s)\le - 4/T_s = -8$ rad/s. Consequently,
% we wish to ensure our closed loop bandwidth is at least 8 rad/s.
% Accordingly, we shape our frequency weighting $W_1(s)$ to reflect our
% desired closed loop bandwidth.
In the following, we refFilter w1 to shape S
n1=2.0101; d1=[1.0000e+00   2.0101e-01]; w1= tf(n1,d1);

figure(1); subplot(211); bodemag(1/w1,'-',logspace(-4,4,50));
title('filter for  S=1/(1+GK)  (performance)')

%  Filter w2 to shape KS 
n2=[9.6785e+00   2.9035e-02   0  0];
d2=[1.0000e+00   1.2064e+04   1.1360e+07   1.0661e+10];
w2=tf(n2,d2);

figure(1); subplot(212); bodemag(1/w2,'-',logspace(-2,4,50));
title('filter for  K/(1+GK)  (control action)')

clear n1 d1 n2 d2;
%% OPTION A: MANUAL UNCERTAIN MISSILE MODEL REPRESENTATION
% In the following we model the uncertain system $G_\Delta(s)$ by
% constructing $M(s)$ and $|\Delta(s)| \le 1$ such that 
% $G_\Delta(s) = LFT(M(s),\Delta(s))$.

ny = 2; % # of measurements
nu = 1; % # of controls

%%
% First, we start by constructing $M_1(s)$ for the original uncertain
% parameter interval
Delta1 = mdiag(deltaZ,deltaM)

% Define state space matrics for M(s)
a=[0 1;0 0];
Bu = [0;1]; Bdelta = eye(2);
Cy = [-1,0; 0,1]; Cdelta = -[1,0;1,0];

M1 = ss(a, [Bdelta,Bu], [Cdelta;Cy], zeros(4,3));
M1 = mktito(M1,ny,nu); % M: (wDelta,u) -> (zDelta,y)
[n1,n2] = size(M1); nzDelta = n1-ny; nwDelta = n2-nu; % signal dimensions

% clear a Bu Bdelta Cy Cdelta n1 n2;
%%
% Second, we use a loop transformation to shift the interval of uncertain
% parameter values to be symmetric about zero.
Delta1Mean = diag(mean([deltaZ.range;deltaM.range],2))
Delta2 = Delta1 - Delta1Mean;
M2 = feedback(M1,-Delta1Mean,[1:nwDelta],[1:nzDelta],-1);
M2 = mktito(M2,ny,nu); % M: (wDelta,u) -> (zDelta,y)

% clear M1 Delta1 Delta1Mean;
%%
% Third, we use a loop transformation to normalize the interval of
% uncertain parameter values to [-1,1]
Delta2Range = diag([deltaZ.range(2)-deltaZ.range(1);...
	deltaM.range(2)-deltaM.range(1)])/2
Delta3 = Delta2Range\Delta2; usample(Delta3)
M3 = M2*mdiag(Delta2Range,eye(nu));
M3 = mktito(M3,ny,nu); % M: (wDelta,u) -> (zDelta,y)

% clear M2 Delta2 Delta2Range;
%%
% Finally, we can construct the uncertain plant model as
% $G_\Delta(s) = LFT(M(s),\Delta(s))$, where LFT is a ower fractional
% transformation interconnection.
deltaG = lft(Delta3,M3); % G_\Delta: (u) -> (y)

%% OPTION B: UNCERTAIN MISSILE MODEL REPRESENTATION VIA MATLAB

% VIA DIRECT UNCERTAINTY ATOMS
a=[-deltaZ 1;-deltaM 0]; Bu = [0;1]; Cy = [-1,0; 0,1];
deltaG=ss(a, Bu, Cy, zeros(2,1));

%%
% To illustrate the variance of the dynamics due to parameter uncertainty, 
% we can plot the uncertain plant frequency response. Notice the magnitude
% and resonant frequency varies widely. This indicates a robust control
% design based on a nominal plant model would be very challenging.
figure(2); bodemag(usample(deltaG,20));

clear a Bu Cy;
%% GENERAL CONTROL PROBLEM FORMULATION

% control together unweighted general control problem
systemnames = 'deltaG';
inputvar = '[r; u]';
outputvar = '[r-deltaG(1); u; r-deltaG(1); deltaG(2)]';
input_to_deltaG = '[u]';
sysoutname = 'deltaPP'; cleanupsysic = 'yes'; sysic;

% weighted general control problem
W = mdiag(w1,w2,eye(ny));
deltaP = mktito(W*deltaPP,2,1); % P: (w,u) -> (z,y)

%% CONSTRUCT CONTROLLER FOR NOMINAL SYSTEM
% Considering the daunting task of robust control design we might be
% tempted to use the nominal system. Although $H_\infty$ control does a
% nice job of meeting our performance specifications, we find that the
% nominal control design is not robust.

[K,CL,gam,info] = hinfsyn(deltaP.nominal,ny,nu,...
	'method','lmi','display','off');

disp(['check performance specs by checking if gamma < 1']);
disp(['gamma = ',num2str(gam)]);

figure(3); step(usample(lft(deltaPP.nominal,K),20));
title('nominal closed loop step response');

figure(4); step(usample(lft(deltaPP,K),20));
title('uncertain closed loop step response');


%% CONSTRUCT 'MORE' ROBUST CONTROLLER VIA HINFSYN
% Given the abysmal robustness of the above controller. Let's reformulate
% our reformulate our control problem to reflect our uncertainty. Similar
% to our uncertain plant description
% $G_\Delta(s) = LFT(M_G(s),\Delta(s))$
% we can rewrite our uncertain system description
% $P_\Delta(s) = LFT(M_P(s),\Delta(s))$

[M_P,Delta]=lftdata(deltaP); % M_P: (w_delta,w,u) -> (z_delta,z,y)
usample(Delta)

[K,CL,gam,info] = hinfsyn(M_P,ny,nu,...
	'method','lmi','display','off');

disp(['check robust performance specs by checking if gamma < 1']);
disp(['gamma = ',num2str(gam)]);

figure(3); step(usample(lft(deltaPP.nominal,K),20));
title('nominal closed loop step response');

figure(4); step(usample(lft(deltaPP,K),20));
title('uncertain closed loop step response');

%% CONSTRUCT ROBUST CONTROLLER VIA DKSYN
% We can improve the robust performance by taking into account the
% structure of the parameter uncertainty. Here, we use D-scaling to a less
% conservative on the robust performance. As a result, we can  
% construct a controller with better robust performance.

opt = dkitopt('NumberofAutoIterations',5,'AutoScalingOrder',1);
[K,clpmu,bnd,dkinfo] = dksyn(deltaP,2,1,opt);

sprintf('D-K iteration controller achieved a bound value of %5.3g',bnd)

figure(5); step(usample(lft(deltaPP,K),20));
title('uncertain closed loop step response');