%% Clearing 
close all
clear all
clc

%% Plant Set-up
s = tf('s');
G0 = [87.8 -86.4; 
      108.2 -109.6];
G = 1/(75*s + 1)*G0;
G = minreal(ss(G));

%% Inverse-based controller
K = 0.7*((75*s + 1)/(s + 1e-5))*inv(G0);

%% Weights
Wp = 0.5*((10*s + 1)/(10*s + 1e-5))*eye(2);
Wi = ((s + 0.2)/(0.5*s + 1))*eye(2);

%% Generalized Plant P (sysic)
systemnames = 'G Wp Wi';
inputvar = '[ydel{2}; w{2}; u{2}]';
outputvar = '[Wi; Wp; -G-w]';
input_to_G = '[u + ydel]';
input_to_Wp = '[G+w]';
input_to_wi = '[u]';
sysoutname = 'P';
cleanupsysic = 'yes'; sysic;

%% Creating the interconnection matrix N
N = lft(P,K);
omega = logspace(-3,3,61); Nf = frd(N, omega);

%% Hinf Controller Synthesis & Hinf norm 
nmeas = 2; ncon = 2;
[K1, CL, GAM, INFO] = hinfsyn(P,nmeas,ncon);
gam2 = norm(CL, inf)

%% Mu for RS
Nrs = Nf(1:2, 1:2);
[mubnds, muinfo] = mussv(Nrs, [1 1; 1 1], 'c');
muRS = mubnds(:,1); [muRSinf,muRSw] = norm(muRS, inf)

