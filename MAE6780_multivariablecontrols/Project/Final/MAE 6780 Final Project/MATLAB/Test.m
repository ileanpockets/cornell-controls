% Eileen J. Kim (ejk93)
% MAE 6780/Cornell Cup USA 
% May 15, 2014

% The purpose of this code is to simulate graphs of the nominal movement
% and synchronization of the walking and weight-shfiting mechanisms in the
% bipedal C-3PO robot. The two graphs output are the motor
% speeds/accelerations and the reference input signal to a control system
% of omega - the angular velocity of the weight-shifting motor.
%% Clearing
close all
clear all
clc

%% Parameter Setup
conv = 2.54*1e-2;   % conversion factor: in -> m
n = 100;            % number of steps
tf = 10;            % final time, [s]
ts = tf/n;          % step-size, [s]
tspan = 0:ts:tf;
T = 1/0.75;         % period of walking/weight-shifting motion, [s/rev]
path = .3048; 
v_avg = 2*path/T;   % average velocity of travel, [m/s]
v_top = 2*v_avg;    % top velocity of travel, [m/s]
a = (v_top)^2/path; % acceleration, [m/s^2], 2.75 m/s^2
r_pin = 1.5/2*conv; % radius of pinion, [m]
%% Wave Formulation
y_square = a*square(2*pi/T*tspan);          % acceleration profile
y_tri = v_top*sawtooth(2*pi/T*tspan, 0.5);  % velocity profile 

%% Plotting
figure(1)
hold on;
h = plot(tspan, y_square, tspan, y_tri);
set(h(1),'LineWidth', 2);
set(h(2),'LineWidth', 2);

plot([T/2 T/2],[-4 4], 'r:')
plot([T T],[-4 4], 'r:')
plot([3*T/2 3*T/2],[-4 4], 'r:')
plot([4*T/2 4*T/2],[-4 4], 'r:')
plot([5*T/2 5*T/2],[-4 4], 'r:')
xlabel('Time [s]', 'Interpreter', 'latex')
annotation('textarrow',[0.3,0.255],[0.4,0.55], 'String', 'Leg 1 Raises', 'Color', 'r', 'FontWeight', 'bold', 'Interpreter', 'latex')
annotation('textarrow',[0.6,0.52],[0.4,0.55], 'String', 'Leg 1 Drops', 'Color', 'r', 'FontWeight', 'bold', 'Interpreter', 'latex')
title('Timing simulation', 'Interpreter', 'latex')
axis([0 4 -3.5 3.5])
legend('Acceleration', 'Velocity', 'Walking');
hold off;

%% Setting up reference input for simulink
r = y_tri/r_pin;    % omega = v/r

% Plot
figure(2)
plot(tspan, r);
xlabel('Time [s]')
ylabel('\omega [rad/s]')
title('Reference Input to System')
axis([0 10 -55 55])