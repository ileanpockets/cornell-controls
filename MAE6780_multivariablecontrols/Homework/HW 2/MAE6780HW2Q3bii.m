function MAE6780HW2Q3bii
%Clearing
close all
clear all
clc

%% Problem Set-Up
A = [0 1 0; 980 0 -2.8; 0 0 -100];
B = [0 0 100]';
Cz = [eye(3); 0, 0, 0];
Dzu = [0 0 0 1]';
x0 = [0.01; 0; 0];

epsilon = 1e-18;
[p,n] = size(A);
[n2,m] = size(B);

%% Find continuous-time Lyapunov function V(x) = x'*X*x verifying stability
% for control law u(t) = Fx(t)
cvx_begin sdp quiet
    variable T(n,n) symmetric
        variable Y(m,n2)
        variable gamma1(1)
        minimize gamma1
        subject to
       [((A*T + B*Y) + (A*T + B*Y)'), (Cz*T + Dzu*Y)'; (Cz*T+Dzu*Y), -eye(4)]<=0;
        T >= epsilon*eye(n2);
        [gamma1, x0'; x0, T] >=epsilon*eye(4);
cvx_end
F = Y/T;
disp(eig(A + B*F));
gamma1
% initial(ss(A+B*F, [], Cz, Dzu, x0)); 

%% Riccati equation
[K,P,e] = lqr(A, B, eye(3),1);
gamma_prime = x0'*P*x0