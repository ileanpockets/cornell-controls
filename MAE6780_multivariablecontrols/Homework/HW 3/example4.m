
%%
% We wish to design a controller yielding closed loop sensitivity
% $S(s)=E(s)/R(s)$ and $T(s)=Y(s)/R(s)$ satisfying frequency domain
% specifications $|S(j\omega)|<f(\omega)$ and $T(j\omega)|<g(\omega)$. In
% the following example, we will use a $H_\infty$ to shape the closed loop
% sensitivity. The basic idea entails pushing the closed loop sensitivity
% down at critical frequencies by making its respective frequency weighting
% 'large' at those frequencies. Here, we present a methodical approach for
% meeting closed loop specifications.

close all;
%% DEFINE THE PLANT DYNAMICS
s= tf([1,0],1);
Gsys = 1/(s+1)/(s+3);
W = [-3,4]; V = [10^(W(1)),10^(W(2)),-60,60]; % semilogx axis bounds
w = logspace(W(1),W(2),200);
Ksys = 1;

%% STEP 0: CLOSED LOOP SPECIFICATIONS

%%
% 2 percent tracking error for $\omega \in [0,\omega_T]$
e_T=0.05; e_TdB = 20*log10(e_T); w_T=0.1;
%%
% 1 percent percent noise rejection error for $\omega \in [\omega_NR,\infty]$
e_NR=0.05; e_NRdB = 20*log10(e_NR); w_NR=500;

%% STEP 1: PLOT CLOSED LOOP SPECIFICATIONS

%%
% First, plot error tracking specifications for closed loop sensitivity
% $S(s)$. % Second, plot noise rejection specifications for closed loop complementary
% sensitivity $T(s)$.

figure(1); subplot(2,1,1);
semilogx(1,1); axis([w(1),w(end),e_TdB-20,+20]);
ylabel('magnitude [dB]'); xlabel('frequency [rad/s]'); 
title('e_{T} and S(s) Specification');
VV=axis; grid on; hold on;
fill([VV(1),VV(1),w_T,w_T,VV(1)],[VV(4),e_TdB,...
	e_TdB,VV(4),VV(4)],[0.5,0,0]); % e_T specification box

subplot(2,1,2);
semilogx(1,1); axis([w(1),w(end),e_NRdB-20,+20]);
ylabel('magnitude [dB]'); xlabel('frequency [rad/s]'); 
title('e_{NR} and T(s) Specification');
VV=axis; grid on; hold on;
fill([w_NR,w_NR,VV(2),VV(2),w_NR],[VV(4),e_NRdB,...
	e_NRdB,VV(4),VV(4)],[0,0,0.5]); % e_NR specification box

%% STEP 2: CHOOSE FREQUENCY WEIGHTINGS
% In the following, we choose ferquency weightings $W_e(s)$ and $W_n(s)$
% that lie within our closed loop specifications. As a basic rule of
% thumb, we choose transfer functions that are bigger rather than
% smaller and fewer states rather than more states. Why?

%%
% Choose $1/W_e(s)$ to lie within our closed loop sensitivity $S(s)$
% specifications. It helps if you either choose $1/W_e(s)$ directly or
% flip the closed loop specification plot and find $W_e(s)$.
We = 30/(s/1e-1+1); % sensitivity weighting for $S(s)$ 

%%
% Choose $1/W_n(s)$ to lie within our closed loop compementary sensitivity
% $T(s)$ specifications. 
Wn = 1e-1*(s/2e0+1)/(s/1e3+1); % sensitivity weighting for $S(s)$

figure(2); subplot(2,1,1);
bodemag(1/We,w);
title('e_{T} and S(s) Specification');
VV=axis; grid on; hold on;
fill([VV(1),VV(1),w_T,w_T,VV(1)],[VV(4),e_TdB,...
	e_TdB,VV(4),VV(4)],[0.5,0,0]); % e_T specification box

subplot(2,1,2);
bodemag(1/Wn,w);
title('e_{NR} and T(s) Specification');
VV=axis; grid on; hold on;
fill([w_NR,w_NR,VV(2),VV(2),w_NR],[VV(4),e_NRdB,...
	e_NRdB,VV(4),VV(4)],[0,0,0.5]); % e_NR specification box

%% STEP 3: SYNTHESIZE $H_\infty$ CONTROL
% In the following, use $H_\infty$ algorithms to synthesize a controller
% minimizing the mixed closed loop sensitivity
% \gamma=\|\begin{bmatrix} W_eS \\ W_nT \end{bmatrix} \|_{H_\infty}
% The basic idea is to push the sensitivity function down at critical
% frequencies by making the respective frequency weighting 'large' at those
% frequencies.
 
W1 = We;
W2 = [];
W3 = Wn;
[Ksys,CL,GAM,INFO] = mixsyn(Gsys,W1,W2,W3);

%% STEP 4: VERIFY CLOSED LOOP SPECIFICATIONS

%%
% We can check our closed loop specifications two ways: $H_\infty$ norm
% calculations and plotting the frequency response. The basic idea being we
% perform more detailed checks as we need to fine tune our design. First,
% we check whether the ${H_\infty}$ norm of our closed loop system is less
% than one. 

norm(CL,inf)

%%
% Suppose we do not meet our specification. We may get additional insight
% into any bottlenecks by plotting the frequency response of the
% mixed closed loop sensitivies. Here, we use basic property of maximum
% singular value,
% $\sigma_{max} ([(W_eS)^*,(W_nT)^*])^2 \le
% \sigma_{max}(W_eS)^2+\sigma_{max}(W_nT)^2$

Ssys = feedback(1,Gsys*Ksys,-1);
Tsys = feedback(Gsys*Ksys,1,-1);
figure(3);
sigma(CL,We*Ssys,Wn*Tsys); legend('mixed','W_eS','W_nT'); grid on;

%% 
% The basic idea is that we take note of the critical frequencies where the mixed
% sensitivy
% $\sigma_{max} ([(W_eS)^*,(W_nT)^*])$ is large and which weighted close
% sensitivites are largest at those critical frequencies. For example, our
% mixed sensitivity is largest for lower frequencies due to 
% $\sigma_{max}(W_eS)$. This would lead us to tune the design by tightening
% the weighting function $W_e$ at those frequencies. The following plot
% verifies the mixed sensitivity specification.

figure(4);
subplot(2,1,1);
bodemag(Ssys,w); hold on;
bodemag(1/We,w); legend('S(s)','1/W_e(s)');
VV=axis; grid on; hold on;
fill([VV(1),VV(1),w_T,w_T,VV(1)],[VV(4),e_TdB,...
	e_TdB,VV(4),VV(4)],[0.5,0,0]); % e_T specification box

subplot(2,1,2);
bodemag(Tsys,w); hold on;
bodemag(1/Wn,w); legend('T(s)','1/W_n(s)');
fill([w_NR,w_NR,VV(2),VV(2),w_NR],[VV(4),e_NRdB,...
	e_NRdB,VV(4),VV(4)],[0,0,0.5]); % e_NR specification box
title('T(s) Verification E(s)/R(s)');

