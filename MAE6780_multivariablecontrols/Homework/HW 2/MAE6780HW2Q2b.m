function MAE6780HW2Q2b

%%Clearing
close all
clear all
clc

%% Set-up
a = 1.5; % 1 < a < 2
t = 0:100;
A = [(-1+a*cos(t)^2), (1-a*sin(t)*cos(t));
(-1-a*sin(t)*cos(t)), (-1 + a*sin(t)^2)];
Phi = [e^((a-1)*t)*cos(t), e^(-t)*sin(t);
    -e^((a-1)*t)*sin(t), e^(-t)*cos(t)];
eig(A*Phi)

