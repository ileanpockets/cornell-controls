
% Eileen J. Kim

% Clearing
close all
clear all
clc

%% Component Specifications
conv = 2.54*1e-2;% 1 in -> 2.54 cm -> 0.0254 mconversion
    %setting conv = 1 will put all the dimensions into inches.

% Rack
w_rack = 0.25*conv; %width, 
h_rack = 0.25*conv; %height,
l_rack = 17*conv; %length, cm
v_rack = w_rack*h_rack*l_rack; %volume, cm^3
rho_steel = 7.87; % density, g/cm^3, from matweb 
m_rack = rho_steel*v_rack/1000; % rack mass, kg

% Pinion
r_pinOR = 1.5/2*conv; % outer radius, cm
r_pinIR = 3/16*conv; % inner radius, cm
r_pinSR = 0.5625*conv; % shaft radius, cm
h_pin = 5/8*conv; % overall length, cm
l_pin = 1/4*conv; % face width, cm
ls_pin = (h_pin - l_pin); %shaft face width, cm
v_pin = pi*(r_pinOR^2 - r_pinIR^2)*l_pin + pi*(r_pinSR^2 - r_pinIR^2)*ls_pin; % volume, cm^3
m_pin = rho_steel*v_pin; % pinion mass, g

% moments of inertia: http://www.cnc.info.pl//files/tecmtrsiz_155.pdf 
J_px = 1/8*m_pin*((2*r_pinOR)^2 + (2*r_pinIR)^2);
J_py = 1/4*m_pin*(((2*r_pinOR)^2+(2*r_pinIR)^2)/4 + h_pin^2/3);
J_pin = J_px;

% Load (for weight-shifting)
w_load = 1.5*conv; % plate width, cm
l_load = 3*conv; % plate length, cm
h_load = 8*conv; %plate height, cm
m_load = 10; %kg
J_load = 1/12*m_load*(w_load^2 + h_load^2); % Jx

% Motor (Electrical Components)
Ke = 7.2; %back emf constant, V/kRPM
Kt = 9.7; % torque constant, oz-in/amp
Ra = 0.84; %DC armature resistance, ohms
La = 0.44; %DC armature inductance, mH
b = 0.25; % viscous frictioncoefficient, Nms, guessed

% Motor & Encoder (Mechanical Components)
m_mot = 1.45; %kg
r_mot = 2.05/2*conv; %m
h_mot = 6.31*conv+33/1000; %m [motor + encoder]
J_motx = 0.5*m_mot*r_mot^2;
J_moty = 1/12*m_mot*(3*r_mot^2 + h_mot^2);
J_mot = J_motx;
eta =0.95; %efficiency, randomly chosen
p = 1.5*conv; %pitch, [m]
z = 36; % numbr of teeth
% pinion specs: http://www.mcmaster.com/#6325k65/=rxp0m6


delta_n = 2.8; % change in speed, [m/s]
delta_t = 1; % change in time [s]
a  = 2.8; % [m/s^2]
% delta_n/delta_t = acceleration
F = 1; % load force (output)
T_in = p*z/(2*pi)*F/eta; 
% T_ina = (J_mot + J_enc + J_pin + (m_load+m_rack)/eta*(p^2*z^2)/(4*pi^2))*pi/30*delta_n/delta_t;%torque for constant acceleration
T_ina = (J_mot + J_pin + (m_load+m_rack)/eta*(p^2*z^2)/(4*pi^2))*pi/30*a;
T = T_ina; % T = T_in + T_ina?
J = J_pin + J_load + J_mot; %is this correct?
g = 9.81; %gravity, m/s^2
% Motor Torque Calculations (http://www.leadshine.com/Pdf/Calculation.pdf)
% http://www.orientalmotor.com/technology/articles/motor-sizing-calculations.html
%for now, model rack and pinion the same way most people do: with the motor
%horizontal.
alpha = pi/2; %anggle of inclination - 90degrees: completely vertical
mu = 1; %frictional coefficinet of sliding surfaces
%draw FBD?

% t1 = 1;%acceleration(deceleration timing - how fast motor accelerates/decelerations
% Nm = 1; %operation speed (top speed in our case)
% i = 1; %gear ratio
% Ta = (J_rot*i^2 + J_load)/9.55*(Nm/t1);%brushles motors

% J_rot: rotor inertia - same as motor inertia?
T_L = ((m_load+m_mot)*g*(2*r_pinOR)*(sin(alpha) + mu*cos(alpha)))/(2*eta); % load torque

%% Parameter Setup
conv = 2.54*1e-2;   % conversion factor: in -> m
% n = 100;            % number of steps
% Tf = 10;            % final time, [s]
% ts = Tf/n;          % step-size, [s]
% tspan = 0:ts:Tf;
Tw = 1/0.75;         % period of walking/weight-shifting motion, [s/rev]
path = .3048; 
v_avg = 2*path/Tw;   % average velocity of travel, [m/s]
v_top = 2*v_avg;    % top velocity of travel, [m/s]
a = (v_top)^2/path; % acceleration, [m/s^2], 2.75 m/s^2
r_pin = 1.5/2*conv; % radius of pinion, [m]
% velocity = v_top*sawtooth(2*pi/Tw*tspan, 0.5);  % velocity profile 

%% Motor Controller Dynamics
s = tf('s');
% TF_motor = Kt/(La*s + Ra);
% TF_load = 1/(J*s + b);
% [A,B,C,D] = linmod('ProjectModel2');
% Gsys = ss(A, B, C, D);

% d/dt(omega) = -(b/J)omega + (Kt/J)i - T_L/J;
% d/dt(i) = -(Ke/La)omega - (Ra/La)i + (1/La)Va;

% x = [theta; thetadot; i]
% u = [Va; T_L]
% y = thetadot

% A_21 = (-Ke*Kt+b*Ra)/(J*La);
% A_22 = (-J*Ra + La*b)/(J*La);
% A = [0 1; A_21 A_22];
A = [0 1 0;
     0 -b/J Kt/J;
     0 -Ke/La -Ra/La];
B = [0 0;
     0 -1/J;
     1/La 0];
 
% B = [0 0; Kt/(J*La) -1/J];
%B = [0; Kt/(J*La)]
% C = [1 0];
C = [0 1 0];
D = [0 0];
Gsys = ss(A, B, C, D);
%To figure out the pulse generation need:
%- the time it takes to complete one 
%is this the same pulse generation 
% r = T_L; %-> modelled in the lsim later on as teh referene input?
%can also use pulse generator block in simulink for this

%better tracking - increase P; less efort, increase R

%
%% MPC (Constrained?)
% Constraints: constraint on voltage: upper and lower limits - limit it to
% 24 V 
% code taken heavily from hw 1, problem 2c
%% Define discrete-time system of the DC motor
Ts = 0.01; % sampling time
Tp = 0.5; % [s] choose prediction horizion length
Tf = 5;

Gsys = ss(A,B,C,0);
Gdsys = c2d(Gsys,Ts);

%% SIMULATION PARAMETERS
x0 = [0; 0; 0; 0; 0; 0.5]; %arbitrary last start reference ->> CHANGE THIS LATER
% x0 = [0 0 0 0 0]'; %without reference input
% Tf=5; % [s] final time of simulation
tspan = 0:Ts:Tf;
%Tw = period of walking motor
velocity = v_top*sawtooth(2*pi/Tw*tspan, 0.5); % velocity/r_pin = r = reference input
acceleration = a*square(2*pi/Tw*tspan); 
% acceleration2 = T*square(2*pi/T*tspan);
% size(velocity)
r = velocity/r_pin;
r2 = acceleration/r_pin;
% [~,i2] = size(r(1,:));
% delta_v = zeros(1,i2-1);
% size(delta_v)
% % Creating delta_v: a vector of the change in velocity of the
% % weight-shifting motor at each time step = Ts = 0.01 - this is the same
% % timestep that the MPC simulation is run - giving us the values which line
% % up with each step k.
% for j = 1:i2-1
%     delta_v(1,j) = velocity(1,j+1) - velocity(1,j);
% end
% % now... how to insert this into the state-space matrices so that we can
% % get changing values for r(k+1) instead of r(k+1) = r(k)
% % r(k+1) = r(k) + delta_v/r_pin = r(k) + (thetadot(k) - thetadot(k-1))?
% % should i expand the state-space matrices to include thetadot(k-1)?
% %% AUGMENTED MATRICES
% % %% with r_k at end
% A_tilde = [Gdsys.A Gdsys.B zeros(3,1); [0 0 0 1 0 0; 0 0 0 0 1 0; 0 0 0 0 0 1]];
% % A_tilde = [Gdsys.A Gdsys.B zeros(3,1); [0 0 0 1 0 0; 0 0 0 0 1 0; 0 0 0 0 0 1]];
% B_tilde = [Gdsys.B; eye(2); 0 0];
% C_tilde = [0 1 0 0 0 -1];
% 
% %% without r_k at end
% % x_tilde = [theta; thetadot; i; Va; T_L]
% % v_tilde = [vk_va; vk_tl]
% % delta_v = v(k+1)-v(k);
% 
% % A_tilde = [Gdsys.A Gdsys.B; [0 0 0 1 0; 0 0 0 0 1]];
% % B_tilde = [Gdsys.B; eye(2)];
% % C_tilde = [0 1 0 0 0];
% 
% %% New discretized system, G_tilde_dsys with augmented matrices
% Gdtsys = ss(A_tilde, B_tilde, C_tilde, 0, Ts);
% clear Gsys A B C 
% 
% %weights
% % W = 
% MPCobj = mpc(Gdtsys, Ts, Tp, 2);
%%
h1 = tf(Kt, [La Ra]);
h2 = tf(1, [J b]);
dcm = ss(h2)*[h1, 1];
dcm = feedback(dcm, Ke, 1, 1);


%plot angular velocity response to a step change in voltage Va
figure(1)
stepplot(dcm(1))
% 
% %plot angular velocity response to triangle wave change in voltage Va
% figure(2)
% lsim(dcm(1), r, tspan) % useless because we want to simulate a triangle
% wave in OMEGA, VELOCITY, NOT VOLTAGE

%% Feedforward
Kff = 1/dcgain(dcm(1)); %7.22
Td = -J*r2;
t = tspan;
u = [r;Td];

cl_ff = dcm*diag([Kff,1]); % 2 inputs
set(cl_ff,'InputName',{'w_ref','Td'},'OutputName','w');

figure(2)
h = lsimplot(cl_ff, u, t);
title('Setpoint tracking and disturbance rejection')
legend('cl\_ff')

% % Annotate plot
% line([5,5],[.2,.3]); line([10,10],[.2,.3]);
% text(7.5,.25,{'disturbance','T_d = -0.1Nm'},...
%             'vertic','middle','horiz','center','color','r');

%% LQR Control Design
% To further improve performance, try designing a linear quadratic
% regulator (LQR) for the feedback structure shown below.  
%
% <<../Figures/dcdemofigures_05.png>>
%
% In addition to the integral of error, the LQR scheme also uses the
% state vector x=(i,w) to synthesize the driving voltage Va.  The
% resulting voltage is of the form
%
%       Va = K1 * w + K2 * w/s + K3 * i
%
%       where i is the armature current.

    
%%
% For better disturbance rejection, use a cost function that penalizes
% large integral error, e.g., the cost function 
%
% $$ C = \int^\infty_0 (20q(t)^2+\omega(t)^2+0.01V_a(t)^2) dt $$
%
% where
%
% $$ q(s) = \omega(s)/s. $$
%
% The optimal LQR gain for this cost function is computed as follows:

dc_aug = [1 ; tf(1,[1 0])] * dcm(1); % add output w/s to DC motor model
 
K_lqr = lqry(dc_aug,[1 0;0 20000],0.01);
 
%%   
% Next derive the closed-loop model for simulation purposes: 

P = augstate(dcm);                     % inputs:Va,Td  outputs:w,x
C = K_lqr * append(tf(1,[1 0]),1,1);   % compensator including 1/s
OL = P * append(C,1);                  % open loop

CL = feedback(OL,eye(3),1:3,1:3);      % close feedback loops
cl_lqr = CL(1,[1 4]);                  % extract transfer (w_ref,Td)->w

figure(3)
lsimplot(cl_lqr,u,t)
%%
% This plot compares the closed-loop Bode diagrams for the three designs
figure(4)
bodeplot(cl_ff,cl_lqr);
legend('cl\_ff', 'cl\_lqr');

figure(5)
lsimplot(cl_ff,cl_lqr,u,t);
legend('feedforward','feedback LQR')
