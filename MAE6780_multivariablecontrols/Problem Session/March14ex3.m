function March14ex3
% See MAE 6780 lecture notes for 3/14/14, example 3
% See posted code (misldem.m)
% run program, then type in Pbig, Gsys, Ksys, or CLsys in MATLAB workspace

% Clearing
clear all
close all
clc


Gsys = zpk([], 0, 1); % G = 1/s;
Ksys = zpk([], [], 1); % K = 1

%% OPEN LOOP SYSTEM PBIG
systemnames = 'Gsys'; % these are not the transfer functions themselves but the outputs to them
inputvar = '[r-Gsys{1}; u{1}]'; %number in curly brackets tell you MIMO OR SISO, in this case - SISO
outputvar = '[r-Gsys(1); u; r-Gsys(1); Gsys(2)]'; % {z1;z2;y}
input_to_Gsys = '[u]';
sysoutname = 'Pbig'; cleanupsysic = 'yes'; sysic

