function MAE6780HW2Q4Observer
% MAE 6780 HW 2, Q4
% Eileen J. Kim (ejk93)
% March 26, 2014

% Clearing 
close all
clear all
clc

% Set-up
% Original Matrices
A = [0 1 0;
    980 0 -2.8;
    0 0 -100];
B = [0 0 100]';
Cz = [10 1 0; 
    0 0 0];
Cy = [1 0 0];
Dzu = [0 1]';

x0 = [-0.015 0 0]';

% Augmented Matrices
% A_til = [A, zeros(3,1); -Cy 0]; %4x4 
% B_til = [B; 0]; %4x1
% Br_til = [zeros(3,1); 1]; %4x1
% [m_b, n_b] = size(B_til); %4x1
[m_b, n_b] = size(B); %3x1
% x0_til = [x0; 0]; %4x1
sig = 500;

% Continuous-time state feedback LMI
% u(t) = -K1*x(t) - K2*z(t)
cvx_begin sdp quiet
    variable X(m_b,m_b) symmetric %3x3
    variable H(m_b, n_b) %3x1
    variable c
    (X*A - H*Cy) + (X*A - H*Cy)' + 2*sig*eye(m_b)*X <=-c*eye(size(A));
    X > 0
    minimize(-c)
cvx_end

L = inv(X)*H
L(1)
disp(eig(A - L*Cy)); % display closed loop poles

% Plot figure
figure(1); clf;
tf = .01;
initial(ss(A - L*Cy, [], Cy, []), x0, tf); ylabel('response')
% u = -K_til*x0_til