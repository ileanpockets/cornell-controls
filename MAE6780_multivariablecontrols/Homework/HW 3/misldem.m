z%MISLDEM   Gain-scheduling control of a missile autopilot (demo)
% 
%See also  LMIDEM, RADARDEM, SATELDEM

% Author: P. Gahinet
% Copyright 1995-2004 The MathWorks, Inc.
%       $Revision: 1.1.8.1 $
echo off
clc

disp(' ');
disp(' ');
disp(' ');
disp(' ');
disp('                     DEMO "MISSILE" ');
disp('                     ************** ');
disp(' ');
disp(' ');
disp('           GAIN-SCHEDULING OF A MISSILE AUTOPILOT   ');
disp(' ');
disp(' ');
disp(' ');
disp(' ');
disp(' ');
disp('            (see user''s manual for more details)');
disp(' ');
disp(' ');


%  TRACKING LOOP :
%                _______        _______
%   r        e   |     |        |     |-----------+----> azv 
%   ----->O----->|  K  |------->|  G  |           |
%         |-     |     |   u    |_____|----+      |
%         |   +->|_____|                   | q    | 
%         |   |                            |      |
%         |   |____________________________|      |
%         |_______________________________________|
%
%
%  Goal: settling time < 0.5 sec. for the step response of the
%        vertical acceleration azv 
%        
%  Loop-shaping formulation:
%
%         ||  w1 S   ||                          -1
%         ||         ||   <  1 ,       S = (1+GK) 
%         ||  w2 KS  ||oo
%  CHOICE OF SHAPING FILTERS:
%


%% UNCERTAIN MISSILE MODEL

%  Specify the range of parameter values (parameter box)  
Zmin=.35;  Zmax=4.35;
Mmin=-365;   Mmax=380;
deltaZ=ureal('deltaZ',mean([Zmin Zmax]),'Range',[Zmin Zmax]);
deltaM=ureal('deltaM',mean([Mmin Mmax]),'Range',[Mmin-50 Mmax+50]);

% describe uncertain model as uG=LFT(Delta,G)
Delta=mdiag((deltaZ-deltaZ.nominal)/(Zmax-Zmin),...
    (deltaM-deltaM.nominal)/(Mmax-Mmin) ); % define |Delta|<1
a=[0 1;0 0]+mean([Zmin Zmax])*[-1 0;0 0]+mean([Mmin-.1 Mmax+.1])*[0 0;-1 0];
bu=[0;1]; bdelta=eye(2); b=[bdelta,bu];
cy=[-1 0;0 1]; cdelta=mdiag(Zmax-Zmin,Mmax-Mmin)*[-1,0;-1,0]; c=[cdelta;cy];
G=ss(a, b, c, zeros(4,3));
uG_LFT=lft(Delta,G);

% use uncertain model description in robust control toolbox
a=[-deltaZ 1;-deltaM 0];
uG=ss(a, bu, cy, zeros(2,1));

% show methods are equivalent
[b,samples]=usample(uG,40);
figure(1);
bodemag(usubs(uG,samples),'b-'); hold on;
bodemag(usubs(uG_LFT,samples),'r:');

%% Definition of Performance Weightings
%  Filter w1 to shape S (sensitivity)
n1=2.0101; d1=[1.0000e+00   2.0101e-01];
w1=tf(n1,d1);

figure(2); subplot(211); bodemag(w1,'b-',logspace(-4,6,50));
title('filter for  S=1/(1+GK)  (tracking performance)')

%  Filter w2 to shape KS (input sensitivity)
n2=[9.6785e+00   2.9035e-02   0  0];
d2=[1.0000e+00   1.2064e+04   1.1360e+07   1.0661e+10];
w2=tf(n2,d2);

figure(2); subplot(212); bodemag(w2,'b-',logspace(2,4,50));
title('filter for  K/(1+GK)  (input sensitivity)')

int1=tf([1],[1]);


%% INTERCONNECTION OF PERFORMANCE MODEL

systemnames = 'uG int1 w1 w2';
inputvar = '[r; K]';
outputvar = '[w1; w2; r-uG(1); uG(2)]';
input_to_uG = '[int1]';
input_to_int1 = '[K]';
input_to_w1 = '[r-uG(1)]';
input_to_w2 = '[int1]';
sysoutname = 'uncP'; cleanupsysic = 'yes'; sysic;

%% CLASSIC Hinf CONTROLLER SYNTHESIS

[K0,CL,gam0,info]=hinfsyn(uncP.nominal,2,1,'method','lmi');
CL0=lft(uG,K0);

%% MONTE CARLO STABILITY ANALYSIS
% plot stable and unstable points
maxEig=squeeze(max(real(eig(usubs(lft(uncP,K0),samples)))));
ii=find(maxEig>0);
jj=find(maxEig<0);

figure(3);
plot(deltaM.nominal,deltaZ.nominal,'gsq'); hold on;
for k=1:length(jj)
    kk=jj(k);
    plot(samples(kk).deltaM,samples(kk).deltaZ,'bo'); hold on;
end % k=jj
for k=1:length(ii)
    kk=ii(k);
    plot(samples(kk).deltaM,samples(kk).deltaZ,'rx'); hold on;
end % k=ii
xlabel('\deltaM'); ylabel('\deltaZ');
% figure(3); bodemag(usubs(lft(uncP,K0),samples),'r:');

%% ROBUST STABILITY ANALYSIS VIA SMALL GAIN THEOREM

% recall that uG=lft(Delta,G)
% check stability of lft( lft(Delta,G) ,K0*mdiag(-1,1)) using small gain theorem
figure(4);
sigma(lft(G,K0*mdiag(-1,1)));
disp(norm(lft(G,K0),inf));
% stability not verified b/c sigma(lft(G,K0))>1 for some frequencies


%% MU-ANALYSIS (scaled small gain theorem)

[stabmarg,destabunc,report,info] = robuststab(lft(uncP,K0));
disp(report)

% plot scaled singular value plot
figure(5); sigma(info.MussvBnds); title('\mu analysis');


%% MU-SYNTHESIS ROBUST CONTROLLER
frad = logspace(-8,2,500); % frequency grid

% first iteration
opt = dkitopt('NumberofAutoIterations',1,'AutoScalingOrder',0);
[kmu,clpmu,bnd,dkinfo] = dksyn(uncP,2,1,opt);
sprintf('D-K iteration controller achieved a bound value of %5.3g',bnd)
figure(6); sigma(dkinfo{1}.MussvBnds); title('\mu analysis');

% second iteration
opt = dkitopt('NumberofAutoIterations',2,'AutoScalingOrder',0,'InitialController',kmu);
[kmu,clpmu,bnd,dkinfo] = dksyn(uncP,2,1,opt);
sprintf('D-K iteration controller achieved a bound value of %5.3g',bnd)
figure(7); sigma(dkinfo{end}.MussvBnds); title('\mu analysis');

% third iteration
opt = dkitopt('NumberofAutoIterations',2,'AutoScalingOrder',0,'InitialController',kmu);
[kmu,clpmu,bnd,dkinfo] = dksyn(uncP,2,1,opt);
sprintf('D-K iteration controller achieved a bound value of %5.3g',bnd)
figure(8); sigma(dkinfo{end}.MussvBnds); title('\mu analysis');

% fifth iteration
opt = dkitopt('NumberofAutoIterations',3,'AutoScalingOrder',0,'InitialController',kmu);
[kmu,clpmu,bnd,dkinfo] = dksyn(uncP,2,1,opt);
sprintf('D-K iteration controller achieved a bound value of %5.3g',bnd)
figure(9); sigma(dkinfo{end}.MussvBnds); title('\mu analysis');

%% STABILITY AND PERFORMANCE ANALYSIS

% calculate nominal performance
disp(norm(lft(uncP.nominal,kmu),inf))

% WORST PERFORMANCE VIA MONTE CARLO
disp(max(norm(lft(usubs(uncP,samples),kmu),inf)))

% ROBUST STABILITY ANALYSIS
[stabmarg,destabunc,report,info] = robuststab(lft(uncP,kmu));
disp(report)
% plot scaled singular value plot
figure(10); sigma(info.MussvBnds); title('\mu analysis');

% ROBUST PERFORMANCE ANALYSIS
[perfmarg,perfmargunc,report,info] = robustperf(lft(uncP,kmu));
disp(report)
% plot scaled singular value plot
figure(11); sigma(info.MussvBnds); title('\mu analysis');




%% Definition of Performance Weightins
%  Filter w1 to shape S (sensitivity)
n1=2.0101;
d1=[1.0000e+00   2.0101e-01];
w1=ltisys('tf',n1,d1);

figure(101); subplot(211); splot(w1,'bo',logspace(-4,6,50));
title('filter for  S=1/(1+GK)  (performance)')

%  Filter w2 to shape KS (input sensitivity)
n2=[9.6785e+00   2.9035e-02   0  0];
d2=[1.0000e+00   1.2064e+04   1.1360e+07   1.0661e+10];
w2=ltisys('tf',n2,d2);

figure(101); subplot(212); splot(w2,'bo',logspace(2,4,50));
title('filter for  K/(1+GK)  (control action)')

int1=ltisys('tf',[1],[1]);

%%  AFFINE PARAMETER-DEPENDENT MODEL OF THE MISSILE
%
%                      [ -Z_al  1 ]         [ 0 ] 
%           dx/dt  =   [          ]  x   +  [   ]  u
%                      [ -M_al  0 ]         [ 1 ] 
%
%    
%          [ azv ]     [ -1     0 ]
%      y = [     ] =   [          ]  x        (q = pitch rate)
%          [  q  ]     [  0     1 ]
%
%  where the aerodynamical coefficients Z_al and M_al
%  vary in
%
%        0.5 <= Z_al <= 4      0 <= M_al <= 106

%  Specify the range of parameter values (parameter box)  
Zmin=.35;  Zmax=4.35;
Mmin=-365;   Mmax=380;  
pv=pvec('box',[Zmin Zmax ; Mmin Mmax]);

%  Specify the parameter-dependent model with PSYS
s0=ltisys([0 1;0 0],[0;1],[-1 0;0 1],[0;0]);
s1=ltisys([-1 0;0 0],[0;0],zeros(2),[0;0],0);  % Z_al component
s2=ltisys([0 0;-1 0],[0;0],zeros(2),[0;0],0);  % M_al component
pdG=psys(pv,[s0 s1 s2]);

%  Specify the loop-shaping control structure with SCONNECT
[pdP,r]=sconnect('r','e=r-G(1);K','K:e;G(2)','G:K',pdG);
%  Augment with the shaping filters
Paug=smult(pdP,sdiag(w1,w2,eye(2))); Paug=smult(sdiag(int1,1),Paug);


%% SELF-SCHEDULED CONTROLLER
%  LMI-BASED SYNTHESIS OF THE LPV CONTROLLER
%    Minimization of gamma for the loop-shaping criterion
[Gopt1,pdK1]=hinfgs(Paug,r,0,1e-2);
[pdP,r]=sconnect('r','e=r-G(1);K','K:int1;G(2)','G:K',pdG,'int1:e',int1);
pCL1=slft(pdP,pdK1);

%% GAIN-SCHEDULED CONTROLLER
beta=eye(4); gopt=[];
for i=1:4
    gopt(i)=norminf(psinfo(slft(Paug,pdK1),'sys',i));
    figure(401); sigma(lmi2ss(psinfo(slft(Paug,pdK1),'sys',i)),'b-'); hold on;
	figure(402); sigma(lmi2ss(psinfo(Paug,'sys',i))); hold on;

	% local controller design
	[K(:,:,i),CLss,gam(i),info]=hinfsyn(lmi2ss(psinfo(Paug,'sys',i)),2,1,'method','lmi');
	figure(401); sigma(CLss,'r-'); hold on;
	gam(i)=norm(lft(lmi2ss(psinfo(Paug,'sys',i)),K(:,:,i)),inf);
end % for i
disp([gopt;gam;(gam-gopt)./gam]);

% GAIN-SCHEDULED CONTROLLER INTERPOLATION
[Gopt2,pdJ,pdQ] = bmh_gs(Paug,r,K,eye(4));
pdK04=slft(pdJ,zeros(1,2));
pdK4=slft(pdJ,pdQ);
pCL4=slft(pdP,pdK04);
% quadperfb(slft(Paug,pdK4));

%% ROBUST GAIN-SCHEDULED CONTROLLER
beta=eye(4); gopt=[];
for i=1:4
    gopt(i)=norminf(psinfo(slft(Paug,pdK1),'sys',i));
    figure(401); sigma(lmi2ss(psinfo(slft(Paug,pdK1),'sys',i)),'b-'); hold on;
	figure(402); sigma(lmi2ss(psinfo(Paug,'sys',i))); hold on;

	% local controller design
	[K(:,:,i),CLss,gam(i),info]=hinfsyn(lmi2ss(psinfo(Paug,'sys',i)),2,1,'method','lmi');
	figure(401); sigma(CLss,'r-'); hold on;
	gam(i)=norm(lft(lmi2ss(psinfo(Paug,'sys',i)),K(:,:,i)),inf);
end % for i
disp([gopt;gam;(gam-gopt)./gam]);

% GAIN-SCHEDULED CONTROLLER INTERPOLATION
[Gopt2,K2] = bmh_hinfgs(Paug,r,K,beta);
pdK2=psys([ss2lmi(K2(:,:,1),1),ss2lmi(K2(:,:,2),1),ss2lmi(K2(:,:,3),1),ss2lmi(K2(:,:,4),1)]);
pCL2=slft(pdP,pdK2);
% quadperf(slft(Paug,pdK2));



%% ALTERNATIVE UNCERTAIN MISSILE MODEL
int1=lmi2ss(int1); w1=lmi2ss(w1); w2=lmi2ss(w2);
systemnames = 'uG int1 w1 w2';
inputvar = '[r; K]';
outputvar = '[w1; w2; int1; uG(2)]';
input_to_uG = '[K]';
input_to_int1 = '[r-uG(1)]';
input_to_w1 = '[int1]';
input_to_w2 = '[K]';
sysoutname = 'uncP'; cleanupsysic = 'yes'; sysic;
int1=ss2lmi(int1,1); w1=ss2lmi(w1,1); w2=ss2lmi(w2,1);

%% MU-SYNTHESIS ROBUST CONTROLLER
frad = logspace(-8,2,500);
opt = dkitopt('NumberofAutoIterations',5,'AutoScalingOrder',0);
% 'InitialController',kmu1);
% 'FrequencyVector',frad
% [kmu,clpmu,bnd,dkinfo] = dksyn(uncP,2,1,dkinfo,opt);
[kmu,clpmu,bnd,dkinfo] = dksyn(uncP,2,1,opt);
sprintf('D-K iteration controller achieved a bound value of %5.3g',bnd)

pdK3=ss2lmi(kmu,1);
pdK3=psys([pdK3,pdK3,pdK3,pdK3]);
pCL3=slft(pdP,pdK3);
Gopt3=quadperf(slft(Paug,pdK3));

%% SAVE RESULTS

save gsctrl_20080106.mat Paug pdP pdK0 pdK1 pdK2 pdK3 Gopt1 Gopt2 Gopt3 Czeta1 pdK4 pdK04 pdQ
