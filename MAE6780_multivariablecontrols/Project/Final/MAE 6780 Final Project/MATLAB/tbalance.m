% Cornell Cup USA, Spring 2014
% Torso Balancing/ Weight-Shifting Mechanism

% MATLAB code to plot the foot clearance (from the center of mass of C3PO
% to the inner edge of the support polygon (foot)) of C3PO against the
% revolutions/second of the walking motor. Multiple curves are plotted -
% these are different values of masses for the weight-shifting, and the
% line at 0.254m corresponds to a 1" foot clearance. Ideally, a mass and
% walking motor speed would be chosen at or above this clearance line. The
% weight-shifting motor follows a triangular velocity profile (constant
% acceleration/decceleration), the acceleration of which can be found using
% the formula: a = (4*path/T)^2/path. Also, note that the foot speed is
% twice that of the motor speed (2:1 gear ratio). 


%% Clearing
close all
clear all
clc

%% Parameters
h = 1.524;              % height from internal weight to ground [m]
m_weight = 5;           % mass of internal weight [kg]
m_body = 52;            % mass of c3po (without internal weight) [kg]
path = .3048;           % track length [m]
g = 9.81;               % gravity [m/s^2]

n = 1;                  % index initialization

foot_clearance = zeros(10,15);  % distance from edge of foot
x = zeros(10,15);       % distance from absolute center
F = zeros(10,15);       % force
tau = zeros(10,15);     % torque
revps = zeros(10,15); 

%% Calculations
% Set up right now to vary walking motor rps from .1 to 1.5 with given
% mass, and calculate foot clearance.

for rps =.1:.1:1.5 % rps = revs/sec --> corresponds to how many steps/sec?
    
    for m_weight = 1:15     % mass weight: 1 kg -> 15 kg
        
        % Calculating acceleration for weight-shifting
        revps(m_weight,n) = rps;
        T = 1/rps;          % period of one cycle
        v_avg = 2*path/T;   % average velocity of travel
        v_top = 2*v_avg;    % top speed of travel
        a = (v_top)^2/path; % 2ad = vf^2

        x(m_weight,n) = h*a*m_weight/(g*(m_weight+m_body));
        foot_clearance(m_weight, n) = x(m_weight,n) - .006; 
 
        % Calculating power required
        F(m_weight,n) = m_weight*a+m_weight*.2*g;
        tau(m_weight,n) = F(m_weight,n)*.01905;
        
    end

    n = n+1;
end

%% Plotting
figure(1)
hold on;

color_array = [[0 0 0]; [1 0 0]; [0 .5 0]; [.75 .75 0]; [.75 0 .75];...
    [0 1 1]; [1 .4 .6]; [.7 .7 .7]; [1 .5 .25]; [.6 .5 .4]; [0.75 0.3 0.1]];
axis([0 1.3 0 .1])

for m = 5:15
    plot(revps(m,1:(18-m)), foot_clearance(m,1:(18-m)), 'Color', color_array(m-4,:),'LineWidth', 2)
end

xlabel('rps', 'Interpreter', 'latex')
ylabel('foot clearance (m)', 'Interpreter', 'latex')
l1 = line([0; 1.5],[0; 0]);
l2 = line([0; 1.5],[.0254; .0254]);
set(l1, 'Linewidth', 2, 'LineStyle', ':')
set(l2, 'Linewidth', 2, 'LineStyle', ':')% 1 inch line
title('Weight-Shifting Mechanism Spec Determination', 'Interpreter', 'latex')
legend('5kg','6kg', '7kg','8kg','9kg', '10kg', '11kg', '12kg', '13kg', '14kg', '15kg')

hold off;

%% Final Decision
% Mass = 22 pounds (10 kg)
% Walking speed: 1.5 step/s
% Walking motor speed: 0.75 rev/s
% Acceleration of weight-shifting: 2.80 m/s^2
