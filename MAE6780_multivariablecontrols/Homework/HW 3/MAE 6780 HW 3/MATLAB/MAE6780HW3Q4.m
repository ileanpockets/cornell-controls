% Eileen J. Kim (ejk93)
% MAE 6780 HW # 3, Q #4

% Clearing
close all
clear all
clc

%% System Set-up
A = [0 0 1 0;
    1.5 -1.5 0 0.018/pi;
    -12 12 -0.8 -0.108/pi;
    -48.84*pi/180 10.64*pi/180 0 -.014];
B = [0 0;
    0.16 0.6;
    -19 -2.5;
    -0.66*pi/180 -.5*pi/180];

C = [1 0 0 0;
    0 1 0 0];

D = zeros(2,2);

G = ss(A,B,C,D);
s = tf('s');
%% System Specifications
%% 2% tracking error for sinusoidal flight angle commands: eT
e_T = 0.02; e_TdB = 20*log10(e_T); w_T = 0.1;
W = [-3,4]; w = logspace(W(1),W(2),200); %omega
k1 = 100;
W1 = k1*(s/(1e-1)+1)^(-1)*(s/1e0+1)^(-2); %used a W1 similar to Q3, but with different gain

W1 = ss(W1*eye(2));
W2 = 0*eye(2);
W3 = 0*eye(2);

%Plot G with bounding boxes
figure(1); subplot(2,1,1); 
bodemag(1/W1(1,1),w);
title('$e_{T}$ and S(s) Specification, $\theta_r$(t)', 'Interpreter', 'latex');
xlabel('Frequency', 'Interpreter', 'latex')
ylabel('Magnitude', 'Interpreter', 'latex')
VV=axis; grid on; hold on;
fill([VV(1),VV(1),w_T,w_T,VV(1)],[VV(4),e_TdB,...
	e_TdB,VV(4),VV(4)],[0.5,0,0]); % e_T specification box
subplot(2,1,2); hold on;
bodemag(1/W1(2,2),w);
title('$e_{T}$ and S(s) Specification, $\gamma_r$(t)', 'Interpreter', 'latex');
xlabel('Frequency', 'Interpreter', 'latex')
ylabel('Magnitude', 'Interpreter', 'latex')
VV=axis; grid on; hold on;
fill([VV(1),VV(1),w_T,w_T,VV(1)],[VV(4),e_TdB,...
	e_TdB,VV(4),VV(4)],[0.5,0,0]); % e_T specification box

%% Design and Verify a Controller using mixsyn 
% Just for the weighting frequencies, before we augment the system with an
% integrator
[Km, CLm, GAMm, INFOm] = mixsyn(G, W1, W2, W3);
GAMm %why is this not equal to the below expression?
norm(CLm,inf) % should be less than 1

S = feedback(1*eye(2),G*Km,-1);
T = feedback(G*Km,1*eye(2),-1);

figure(2)
subplot(2,1,1);
bodemag(S(1,1),w); hold on;
bodemag(1/W1(1,1),w); 
legend('S(s)', '1/$W_1$(1,1)(s)', 'Interpreter', 'latex');
xlabel('Frequency', 'Interpreter', 'latex');
ylabel('Magnitude', 'Interpreter', 'latex');
VV = axis; grid on; hold on;
fill([VV(1),VV(1),w_T,w_T,VV(1)],[VV(4),e_TdB, e_TdB,VV(4), VV(4)],[0.5,0,0]);
title('S(s) Verification, $\theta_r(t)$', 'Interpreter', 'latex')
subplot(2,1,2);
bodemag(S(1,1),w); hold on;
bodemag(1/W1(2,2),w); legend('S(s)','1/$W_1$(2,2)(s)', 'Interpreter', 'latex');
fill([VV(1),VV(1),w_T,w_T,VV(1)],[VV(4),e_TdB, e_TdB,VV(4), VV(4)],[0.5,0,0]);
title('S(s) Verification, $\gamma_r$(t)', 'Interpreter', 'latex');
xlabel('Frequency', 'Interpreter', 'latex');
ylabel('Magnitude', 'Interpreter', 'latex');

%% zero steady-state tracking error for step input commands: (integrator)

int = ss((1/s)*eye(2));

%% Augmented System (Weighted)
systemnames = 'G W1 W2 W3 int';
inputvar = '[w{2};u{2}]';
outputvar = '[W1; W2; W3; int]';
input_to_G = '[u]';
input_to_W1 = '[w-G]';
input_to_W2 = '[u]';
input_to_W3 = '[G]';
input_to_int = '[w-G]';
sysoutname = 'Paug'; cleanupsysic = 'yes'; sysic;

%% Augmented System (Unweighted)
systemnames = 'G int';
inputvar = '[w{2};u{2}]';
outputvar = '[w-G; u; G;int]'; 
input_to_G = '[u]';
input_to_int = '[w-G]';
sysoutname = 'Paug_uw'; cleanupsysic = 'yes'; sysic;

%% Design a Controller using hinfsyn
nmeas = 2;
ncon = 2;
[Ka,CLa,GAMa, INFOa] = hinfsyn(ss(Paug), nmeas,ncon,'method','lmi'); % Augmented system
GAMa %Hinf norm
sys_aug = lft(Paug, Ka);
sys_aug_uw = lft(Paug_uw, Ka);

%% Plot Frequency Response
tf1 = 5;
tf2 = 50;
n = 100;
dt1 = tf1/n;
dt2 = tf2/n;
tspan1 = 0:dt1:tf1;
tspan2 = 0:dt2:tf2;

%% Verifying zero-steady state error to Step Response
r_step1 = 5*ones(size(tspan1))';
r_step2 = 5*ones(size(tspan2))';
[yauw,tauw,xauw] = lsim(sys_aug_uw, [r_step1 r_step1], tspan1); %unweighted
[ya,ta,xa] = lsim(sys_aug, [r_step2 r_step2], tspan2);
figure(3)
subplot(2,1,1) %Unweighted, \theta
plot(tauw,r_step1-yauw(:,1), 'b',tauw,r_step1, 'g:')
legend('System Response, r-$y_1$', 'Step Input, r', 'Interpreter', 'latex')
title('Augmented (Unweighted) System Response to Step Input, $\theta_r$(t)', 'Interpreter', 'latex');
% subplot(2,1,2) %Weighted
% plot(ta, r_step2-ya(:,1), 'b', ta, r_step2,'g:')
% legend('System Response, r-y', 'Step Input, r')
% title('Augmented (Weighted) System Response to Step Input');
% xlabel('Time [s]');
subplot(2,1,2) % Unweighted, \gamma
plot(tauw,r_step1-yauw(:,2), 'b',tauw,r_step1, 'g:')
legend('System Response, r-$y_2$', 'Step Input, r', 'Interpreter', 'latex')
title('Augmented (Unweighted) System Response to Step Input, $\gamma_r$(t)', 'Interpreter', 'latex');

%% Verifying 2% tracking error for sinusoidal flight angle 
% tspan3 = 0:dt3:tf3;
r_sin = 5*sin(0.1*tspan2)';
r_zero = zeros(size(tspan2))';
[yauw2,tauw2,xauw2]=lsim(sys_aug_uw,[r_zero r_sin],tspan2);
figure(4)
subplot(2,1,1) %Unweighted
% plot(tauw2,r_zero-yauw(:,1), 'b',tauw,r_zero, 'g:')
plot(tauw2,yauw2(:,1))
% legend('System Response, r-y1', 'Zero Input, r')
% title('Augmented (Unweighted) System Response to Zero Input, \theta_r(t)');
title('Augmented (Unweighted) Tracking Error to Zero Input, $\theta_r$(t)', 'Interpreter', 'latex');

% subplot(2,1,2) %Weighted
% plot(ta, r_step2-ya(:,1), 'b', ta, r_step2,'g:')
% legend('System Response, r-y', 'Step Input, r')
% title('Augmented (Weighted) System Response to Step Input');
% xlabel('Time [s]');
subplot(2,1,2) %Unweighted
% plot(tauw2,r_sin-yauw(:,2), 'b',tauw,r_sin, 'g:')
plot(tauw2,yauw2(:,2),'b')
% legend('System Response, r-y2', 'Sine Input, r')
% title('Augmented (Unweighted) System Response to Sine Input, \gamma_r(t)');
title('Augmented (Unweighted) Tracking Error to Sine Input, $\gamma_r$(t)', 'Interpreter', 'latex');

%% Control Effort
figure(5)
subplot(2,1,1); 
plot(tauw, yauw(:,3))
title('Elevator Angle, \delta_e')
subplot(2,1,2)
plot(tauw,yauw(:,4))
title('Flaperon Angle, \delta_f')
