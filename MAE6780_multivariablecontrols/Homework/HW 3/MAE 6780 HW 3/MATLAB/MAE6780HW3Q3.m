% Eileen J. Kim (ejk93)
% MAE 6780 HW # 3

% Clearing
close all
clear all
clc
% 
% TF
omega = {10^-3 10^4};
s = tf('s');
G = 100/(s^2 - 25);

% Design Criteria
e_T = 0.05; e_TdB = 20*log10(e_T); w_T = 0.1; 
e_nr = 0.1; e_nrdB = 20*log10(e_nr); w_nr = 80;
M_T = 20*log10(1 + 1/e_T);
M_nr = 20*log10(e_nr/(1+e_nr));

W = [-3,4];w = logspace(W(1),W(2),200); %omega

%% Part A: Plot CL Specs
%Plot G with bounding boxes
figure(1); subplot(2,1,1);
semilogx(1,1);axis([w(1),w(end),e_TdB-20,+20]);
ylabel('Magnitude [dB]', 'Interpreter', 'latex'); 
xlabel('Frequency [rad/s]', 'Interpreter', 'latex');
title('$e_{T}$ and S(s) Specification', 'Interpreter', 'latex');
VV = axis; grid on; hold on;
fill([VV(1),VV(1),w_T,w_T,VV(1)],[VV(4),e_TdB,...
	e_TdB,VV(4),VV(4)],[0.5,0,0]); % e_T specification box

subplot(2,1,2);
semilogx(1,1); axis([w(1),w(end),e_nrdB-20,+20]);
ylabel('Magnitude [dB]', 'Interpreter', 'latex'); 
xlabel('Frequency [rad/s]', 'Interpreter', 'latex'); 
title('$e_{NR}$ and T(s) Specification', 'Interpreter', 'latex');
VV=axis; grid on; hold on;
fill([w_nr,w_nr,VV(2),VV(2),w_nr],[VV(4),e_nrdB,...
	e_nrdB,VV(4),VV(4)],[0,0,0.5]); % e_NR specification box
hold off;

%% Part B: Design frequency weightings for mixed sensitivity

% 1/W1
k1 = 50;
W1 = k1*(s/(1e-1)+1)^(-1)*(s/1e0+1)^(-2);

figure(2); subplot(2,1,1); 
bodemag(1/W1,w);
title('$e_{T}$ and S(s) Specification', 'Interpreter', 'latex');
xlabel('Frequency', 'Interpreter', 'latex');
ylabel('Magnitude', 'Interpreter', 'latex');

VV=axis; grid on; hold on;
fill([VV(1),VV(1),w_T,w_T,VV(1)],[VV(4),e_TdB,...
	e_TdB,VV(4),VV(4)],[0.5,0,0]); % e_T specification box

% 1/W3
k3 = .02; 
W3 = k3*(s/1.75e0+1)^2/(s/1e2+1)^2;
subplot(2,1,2);
bodemag(1/W3,w);
title('$e_{NR}$ and T(s) Specification', 'Interpreter', 'latex');
xlabel('Frequency', 'Interpreter', 'latex');
ylabel('Magnitude', 'Interpreter', 'latex')
VV=axis; grid on; hold on;
fill([w_nr,w_nr,VV(2),VV(2),w_nr],[VV(4),e_nrdB,...
	e_nrdB,VV(4),VV(4)],[0,0,0.5]); % e_NR specification box

%% Part C: Controller K
W1 = W1;
W2 = 0;
W3 = W3;
G = ss(G);
[Km, CLm, GAMm, INFOm] = mixsyn(G, W1, W2, W3);

% Verify CL specs
norm(CLm, inf) % = GAMm, stable if \leq 1 
GAMm

% Iterate
S = feedback(1,G*Km,-1);
T = feedback(G*Km,1,-1);

% Plot singular values (to see where to edit the transfer functions
figure(3)
sigma(CLm, W1*S, W3*T);
title('Singular Values', 'Interpreter', 'latex');
xlabel('Frequency', 'Interpreter', 'latex');
ylabel('Singular Values' , 'Interpreter', 'latex')
legend('mixed','$W_1$S','$W_3$T', 'Interpreter', 'latex');grid on;

%% Plot Frequency-Weighting Specifications
% S(s), tracking error
figure(4);
subplot(2,1,1);
bodemag(S,w); hold on;
bodemag(1/W1,w); 
title('$e_{T}$ and S(s) Specification', 'Interpreter', 'latex');
xlabel('Frequency', 'Interpreter', 'latex');
ylabel('Magnitude', 'Interpreter', 'latex');
legend('S(s)', '1/$W_1$(s)', 'Interpreter', 'latex');
VV = axis; grid on; hold on;
fill([VV(1),VV(1),w_T,w_T,VV(1)],[VV(4),e_TdB, e_TdB,VV(4), VV(4)],[0.5,0,0]);

% T(s), noise rejection
subplot(2,1,2);
bodemag(T,w); hold on;
bodemag(1/W3,w); 
title('$e_{T}$ and S(s) Specification', 'Interpreter', 'latex');
xlabel('Frequency', 'Interpreter', 'latex');
ylabel('Magnitude', 'Interpreter', 'latex');legend('T(s)','$1/W_n(s)$', 'Interpreter', 'latex');
fill([w_nr,w_nr,VV(2),VV(2),w_nr],[VV(4),e_nrdB,...
	e_nrdB,VV(4),VV(4)],[0,0,0.5]); % e_NR specification box
title('T(s) Verification E(s)/R(s)', 'Interpreter', 'latex');

%% Part D: Zero-Steady State Error

%% General System (Unweighted)
systemnames = 'G';
inputvar = '[w{1};u{1}]';
outputvar = '[w(1)-G;u;G;w(1)-G]';
input_to_G = '[u]';
sysoutname = 'Pgen_uw'; cleanupsysic = 'yes'; sysic;

%% General System with Weightings
systemnames = 'G W1 W2 W3';
inputvar = '[w{1};u{1}]';
outputvar = '[W1;W2;W3;w(1)-G]';
input_to_G = '[u]';
input_to_W1 = '[w(1)-G]';%w(1) = r
input_to_W2 = '[u]';
input_to_W3 = '[G]';
sysoutname = 'Pgen'; cleanupsysic = 'yes'; sysic;

%% Augmented System with Weightings
int = 1/s; %integrator
systemnames = 'G W1 W2 W3 int';
inputvar = '[w{1};u{1}]';
outputvar = '[W1; W2; W3; int]';
input_to_G = '[u]';
input_to_W1 = '[w(1)-G]'; %w(1) = r
input_to_W2 = '[u]';
input_to_W3 = '[G]';
input_to_int = '[w(1)-G]';
sysoutname = 'Paug'; cleanupsysic = 'yes'; sysic;

%% Augmented System (Unweighted)
int = 1/s; %integrator
systemnames = 'G int';
inputvar = '[w{1};u{1}]';
outputvar = '[w(1)-G; u; G;int]'; 
input_to_G = '[u]';
input_to_int = '[w(1)-G]';
sysoutname = 'Paug_uw'; cleanupsysic = 'yes'; sysic;

%% Designing Controller using hinfsyn
nmeas=1; ncon=1;
[Kg,CLg,GAMg, INFOg] = hinfsyn(Pgen,nmeas,ncon); %General system
[Ka,CLa,GAMa, INFOa] = hinfsyn(ss(Paug), nmeas,ncon,'method','lmi'); % Augmented system

% Plot mixed closed loop sensitivity frequency response
sys_gen = lft(Pgen,Kg); %general, weighted
sys_gen_uw = lft(Pgen_uw, Kg);
sys_aug = lft(Paug,Ka); %augmented, weighted
sys_aug_uw = lft(Paug_uw,Ka);

% General Weighted System 
S_gen = feedback(1,G*Kg,-1);
T_gen = feedback(G*Kg,1,-1);

figure(5)
sigma(CLg, W1*S_gen, W3*T_gen); 
legend('mixed','$W_1$S','$W_3$T', 'Interpreter', 'latex'); grid on;

% Augmented Weighted System
S_aug = feedback(1,G*Ka,-1);
T_aug = feedback(G*Ka,1,-1);

figure(6)
sigma(CLa, W1*S_aug, W3*T_aug); 
legend('mixed','$W_1S_{aug}$','$W_3T_{aug}$'); grid on;

% Plot response to step input
tf = 15;
n = 100;
dt = tf/n;
tspan = 0:dt:tf;
r = ones(size(tspan))';
[yauw,tauw,xauw] = lsim(sys_aug_uw, r, tspan);
[yguw,tguw,xguw] = lsim(sys_gen_uw, r, tspan);


% plot system response (subtract reference input off the previous plot, 
%which is the error, to get the actual system response)
figure(7); 
subplot(2,1,1); hold on;
plot(tauw,r-yauw(:,1),tauw,r);
plot_title = title('Augmented System Response to Step Input', 'Interpreter', 'latex');
x_axis_label = xlabel('Time [s]', 'Interpreter', 'latex');
legend('y','r(t)', 'Interpreter', 'latex')
hold off;

subplot(2,1,2); hold on;
plot(tguw,r-yguw(:,1),tguw,r);
plot_title = title('General (Unaugmented) System Response to Step Input', 'Interpreter', 'latex');
x_axis_label = xlabel('Time [s]', 'Interpreter', 'latex');
legend('y','r(t)', 'Interpreter', 'latex')
hold off;

%% Experimenting with the presence of an unstable pole
% Experimentally confirm the closed loop sensitivity limit conveyed in Sec.
% 5.13, Rule 8

%Rule 8: Real open-loop unstable pole in G(s) at s = p. We need high
%feedback gains to stabilize the system and we approximately require
%omega_c > 2p. In addition, for unstable plants we need mag(Gs(p)) >
%mag(Gd,ms(p)). Otherwise, the input may saturate when there are
%disturbances and the plant cannot be stabilized. 

% In this problem, we have an unstable pole at s = 5. Therefore we need
% omega_c > 2(5) = 10 rad/s. Let's see what happens when we have an omega_c
% < 10 rad/s. 
% See (5.70)
% fb = bandwidth(sys_gen_uw)
% closed-loop bandwiddth of minimum closed loop sensitivity is approx
% limited to omega_B > 2*mag(p) = 2*5 = 10 rad/s
figure(8)
margin(S); 
% around 1.5 rad/s currently -> 

