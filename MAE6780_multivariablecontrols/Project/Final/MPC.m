function Motor_Controller()
% Eileen J. Kim

% Clearing
close all
clear all
clc

%% Component Specifications
conv = 2.54*1e-2;% 1 in -> 2.54 cm -> 0.0254 mconversion
    %setting conv = 1 will put all the dimensions into inches.

% Rack
w_rack = 0.25*conv; %width, 
h_rack = 0.25*conv; %height,
l_rack = 17*conv; %length, cm
v_rack = w_rack*h_rack*l_rack; %volume, cm^3
rho_steel = 7.87; % density, g/cm^3, from matweb 
m_rack = rho_steel*v_rack/1000; % rack mass, kg

% Pinion
r_pinOR = 1.5/2*conv; % outer radius, cm
r_pinIR = 3/16*conv; % inner radius, cm
r_pinSR = 0.5625*conv; % shaft radius, cm
h_pin = 5/8*conv; % overall length, cm
l_pin = 1/4*conv; % face width, cm
ls_pin = (h_pin - l_pin); %shaft face width, cm
v_pin = pi*(r_pinOR^2 - r_pinIR^2)*l_pin + pi*(r_pinSR^2 - r_pinIR^2)*ls_pin; % volume, cm^3
m_pin = rho_steel*v_pin; % pinion mass, g

% moments of inertia: http://www.cnc.info.pl//files/tecmtrsiz_155.pdf 
J_px = 1/8*m_pin*((2*r_pinOR)^2 + (2*r_pinIR)^2);
J_py = 1/4*m_pin*(((2*r_pinOR)^2+(2*r_pinIR)^2)/4 + h_pin^2/3);
J_pin = J_px;

% Load (for weight-shifting)
w_load = 1.5*conv; % plate width, cm
l_load = 3*conv; % plate length, cm
h_load = 8*conv; %plate height, cm
m_load = 10; %kg
J_load = 1/12*m_load*(w_load^2 + h_load^2); % Jx

% Motor (Electrical Components)
Ke = 7.2; %back emf constant, V/kRPM
Kt = 9.7; % torque constant, oz-in/amp
Ra = 0.84; %DC armature resistance, ohms
La = 0.44; %DC armature inductance, mH
b = 0.2; % viscous frictioncoefficient, Nms, guessed

% Motor & Encoder (Mechanical Components)
m_mot = 1.45; %kg
r_mot = 2.05/2*conv; %m
h_mot = 6.31*conv+33/1000; %m [motor + encoder]
J_motx = 0.5*m_mot*r_mot^2;
J_moty = 1/12*m_mot*(3*r_mot^2 + h_mot^2);
J_mot = J_motx;
eta =0.95; %efficiency, randomly chosen
p = 1.5*conv; %pitch, [m]
z = 36; % numbr of teeth
% pinion specs: http://www.mcmaster.com/#6325k65/=rxp0m6

a  = 2.8; % [m/s^2]
F = 1; % load force (output)
T_in = p*z/(2*pi)*F/eta; 
T_ina = (J_mot + J_pin + (m_load+m_rack)/eta*(p^2*z^2)/(4*pi^2))*pi/30*a;
T = T_ina; 
J = J_pin + J_load + J_mot; %is this correct?
g = 9.81; %gravity, m/s^2
alpha = pi/2; %anggle of inclination - 90degrees: completely vertical
mu = 1; %frictional coefficinet of sliding surfaces
%draw FBD?
T_L = ((m_load+m_mot)*g*(2*r_pinOR)*(sin(alpha) + mu*cos(alpha)))/(2*eta); % load torque

%% Parameter Setup
conv = 2.54*1e-2;   % conversion factor: in -> m
% n = 100;            % number of steps
% Tf = 10;            % final time, [s]
% ts = Tf/n;          % step-size, [s]
% tspan = 0:ts:Tf;
Tw = 1/0.75;         % period of walking/weight-shifting motion, [s/rev]
path = .3048; 
v_avg = 2*path/Tw;   % average velocity of travel, [m/s]
v_top = 2*v_avg;    % top velocity of travel, [m/s]
a = (v_top)^2/path; % acceleration, [m/s^2], 2.75 m/s^2
r_pin = 1.5/2*conv; % radius of pinion, [m]

%% Motor Controller Dynamics
s = tf('s');
A = [0 1 0;
     0 -b/J Kt/J;
     0 -Ke/La -Ra/La];
B = [0 0;
     0 -1/J;
     1/La 0];
C = [0 1 0];
D = [0 0];
Gsys = ss(A, B, C, D);

%% MPC (Constrained?)
% Constraints: constraint on voltage: upper and lower limits - limit it to
% 24 V 
% code taken heavily from hw 1, problem 2c
%% Define discrete-time system of the DC motor
Ts = 0.01; % sampling time
Tp = 0.5; % [s] choose prediction horizion length
Tf = 5;

Gsys = ss(A,B,C,0);
Gdsys = c2d(Gsys,Ts);

%% SIMULATION PARAMETERS
x0 = [0; 0; 0; 0; 0; 0.5]; %arbitrary last start reference ->> CHANGE THIS LATER
tspan = 0:Ts:Tf;
velocity = v_top*sawtooth(2*pi/Tw*tspan, 0.5);
size(velocity)
r = velocity/r_pin;
[~,i2] = size(r(1,:));
delta_v = zeros(1,i2-1);
size(delta_v)

% Creating delta_v: a vector of the change in velocity of the
% weight-shifting motor at each time step = Ts = 0.01 - this is the same
% timestep that the MPC simulation is run - giving us the values which line
% up with each step k.
for j = 1:i2-1
    delta_v(1,j) = velocity(1,j+1) - velocity(1,j);
end
% now... how to insert this into the state-space matrices so that we can
% get changing values for r(k+1) instead of r(k+1) = r(k)
% r(k+1) = r(k) + delta_v/r_pin = r(k) + (thetadot(k) - thetadot(k-1))?
% should i expand the state-space matrices to include thetadot(k-1)?
%% AUGMENTED MATRICES
%% with r_k at end
A_tilde = [Gdsys.A Gdsys.B zeros(3,1); [0 0 0 1 0 0; 0 0 0 0 1 0; 0 0 0 0 0 1]];
B_tilde = [Gdsys.B; eye(2); 0 0];
C_tilde = [0 1 0 0 0 -1];

%% New discretized system, G_tilde_dsys with augmented matrices
Gdtsys = ss(A_tilde, B_tilde, C_tilde, 0, Ts);
clear Gsys A B C 


%% Define Cost Function
% $J(x,u)=x_N^*P_Nx_N+\sum_{k=t}^{t+N-1} x_k^*Qx_k+u_k^*Ru_k$
[n,m] = size(B_tilde);
Q_e = 1; % increase this to penalize 
Q = C_tilde'*Q_e*C_tilde;
rho = 1;
R = rho*eye(m);
P_e = 1; % increase this to improve tracking error
PN = C_tilde'*P_e*C_tilde; % terminal penalty
Np = ceil(Tp/Gdtsys.ts); % time steps in prediction horizon

%% Recursive solution for MPC controller
P_kplus1 = PN;
for k = (Np-1):-1:0; % recursively work from last stage to first stage
	[K_k,P_k] = onestep(Gdtsys,P_kplus1,Q,R); % initialize control law
	P_kplus1 = P_k; % new terminal cost (cost to go)
end
K0 = -K_k; % control law at stage k=0: u0 = K0*x0
P0 = P_k; % value function at stage k=0: V0(x0) = x0'*P0*x0
Kx = @(x0)[K0*x0]; % define control law function handle
Vx = @(x0)[x0'*P0*x0]; % define value-function function handle


% SIMULATE CLOSED LOOP MPC
Nf = ceil(Tf/Gdtsys.ts); % # of simulation time steps
[X,U,J,Tmpc]=simulate(Gdtsys,Kx,Vx,Nf,x0); % simulate closed loop system
e_ss = X(2,end)-X(6,end); %steady state error
%e_ss = r(end) - x(2,end);
Tmpc = Tmpc*Gdtsys.ts; % convert time steps to seconds;
% size(Tmpc)%1x501
% size(X) %6x501
% size(U) %2x501
figure; plotresults(X,U,J,Tmpc);
title(['\rho = ',num2str(rho),', e_s_s = ',num2str(e_ss)]);

%% SUBFUNCTIONS
function [X_MPC,U_MPC,V_MPC,T]=simulate(sys,Kx,Vx,N,x0)
%% Simulate Model Predictive Controller
% sys: discrete time system
% K: state feedback gain
% P: value function matrix
% Tf: simulation time
% x0: initial condition

A = sys.a; B = sys.b;
[n,m] = size(B);
xk = x0; % initialize simulation variables
X_MPC=ones(n,N); U_MPC=ones(m,N); V_MPC=ones(1,N); 

for k=0:N-1
    % CALCULATE OPEN LOOP SOLUTION
	uk = Kx(xk); % calculate control law
	xkplus1 = A*xk+B*uk; % SIMULATE DIFFERENCE EQUATION
	X_MPC(:,k+1) = xk; U_MPC(:,k+1) = uk; V_MPC(k+1) = Vx(xk);
    %V_MPC(k+1) = 
    % x = [theta, thetadot, i, Va, TL, rk];
    % u = K*x 
    % V = [vk_Va; vk_TL];
    % vk = u(k) - u(k-1) = delta_u
    % V = x'Px
	xk = xkplus1; % prepare for next iteration
end
k = k+1;
X_MPC(:,k+1) = xk; U_MPC(:,k+1) = uk; V_MPC(k+1) = Vx(xk);
T = 0:N; % time span
return;

function plotresults(X,U,J,T)
% PLOT RECEDING HORIZON CONTROL
subplot(2,2,1); stairs(T,X(1,:)); ylabel('\delta z [m]'); xlabel('time [s]');
subplot(2,2,2); stairs(T,X(2,:)); ylabel('\delta i [A]'); xlabel('time [s]');
subplot(2,2,3); stairs(T,U(1,:)); ylabel('\delta v [V]'); xlabel('time [s]');
% MAKE ANOTHER SUBPLOT: stairs(T,U(2,:))
subplot(2,2,4); stairs(T,J); ylabel('value function'); xlabel('time [s]');
return;

function [K_k,P_k] = onestep(sys,P_kplus1,Q,R)
% Optimization of one step cost function
% $J(x,u)$ over $(u_k)$ where
% $J(x,u)= x_k^*Qx_k+u_k^*Ru_k+x_{k+1}^*P_{k+1}x_{k+1}$
		
% sys: discrete-time open-loop state space system
% P_kplus1: value function @ stage k+1: $V_{k+1}(x_{k+1})$
		
% K_k: statefeedback gain @ stage k: $u_k=K_kx_k$
% P_k: value function @ stage k: $V_k(x_k)$
A = sys.a; B = sys.b;
[n,m] = size(B);

K_k = inv(R+B'*P_kplus1*B)*(B'*P_kplus1*A); % ---EDIT THIS LINE---- % derive state feedback gain
P_k = A'*P_kplus1*A + Q-A'*P_kplus1*B*inv(R+B'*P_kplus1*B)*B'*P_kplus1*A; % ---EDIT THIS LINE---- % V(x_k)=x_k^*Px_k

return;