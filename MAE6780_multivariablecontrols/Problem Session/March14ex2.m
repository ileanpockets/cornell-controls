function March14ex2
% See MAE 6780 lecture notes for 3/14/14, example 2
% run program, then type in Pbig, Gsys, Ksys, or CLsys in MATLAB workspace

% Clearing
clear all
close all
clc


Gsys = zpk([], 0, 1); % G = 1/s;
Ksys = zpk([], [], 1); % K = 1
Wu = 1; Wr =1; We=1; Wn=1;

%% OPEN LOOP SYSTEM PBIG
systemnames = 'Gsys Wu Wr We Wn'; % these are not the transfer functions themselves but the outputs to them
inputvar = '[rbar{1}; nbar{1}; u{1}]'; %number in curly brackets tell you MIMO OR SISO, in this case - SISO
outputvar = '[We; Wu; Wr-Gsys-Wn]'; % {z1;z2;y}
input_to_Gsys = '[u]';
input_to_Wu = '[u]';
input_to_Wr = '[rbar]';
input_to_We = '[Wr-Gsys-Wn]';
input_to_Wn = '[nbar]';
sysoutname = 'Pbig'; cleanupsysic = 'yes'; sysic
% Pbig
%% CLOSED LOOP SYSTEM
systemnames = 'Gsys Wu Wr We Wn Ksys';
%didn't finish editing here - should finish
% inputvar = '[r{1}]'; % no longer need u
% outputvar = '[r-Gsys]'; % {z1}
% input_to_Gsys = '[Ksys]'; %input to G is Ksys (which is the output of K)
% input_to_Ksys = '[r-Gsys]';
% sysoutname = 'CLsys'; cleanupsysic = 'yes'; sysic
% 
% %type 1 system: pole at s = 0, zero-tracking error
% 
% % CLOSED LOOP SYSTEM METHOD 2
% CL2sys = lft(Pbig, Ksys);