% Eileen J. Kim (ejk93)
% MAE 6780 Final Project
% May 15, 2014

% Clearing
close all
clear all
clc

%% Component Specifications
conv = 2.54*1e-2;% 1 in -> 2.54 cm -> 0.0254 m conversion
    %setting conv = 1 will put all the dimensions into inches.

% Rack
w_rack = 0.25*conv;                 % width, [m]
h_rack = 0.25*conv;                 % height, [m]
l_rack = 17*conv;                   % length, [m]
v_rack = w_rack*h_rack*l_rack;      % volume, [m^3]
rho_steel = 7.87;                   % density, g/[m^3], from matweb 
m_rack = rho_steel*v_rack/1000;     % rack mass, kg

% Pinion
r_pinOR = 1.5/2*conv;               % outer radius, [m]
r_pinIR = 3/16*conv;                % inner radius, [m]
r_pinSR = 0.5625*conv;              % shaft radius, [m]
h_pin = 5/8*conv;                   % overall length, [m]
l_pin = 1/4*conv;                   % face width, [m]
ls_pin = (h_pin - l_pin);           % shaft face width, [m]
v_pin = pi*(r_pinOR^2 - r_pinIR^2)*l_pin + pi*(r_pinSR^2 - r_pinIR^2)*ls_pin; % volume, [m^3]
m_pin = rho_steel*v_pin/1000;       % pinion mass, [kg]

% moments of inertia: http://www.cnc.info.pl//files/tecmtrsiz_155.pdf 
J_px = 1/8*m_pin*((2*r_pinOR)^2 + (2*r_pinIR)^2);
J_py = 1/4*m_pin*(((2*r_pinOR)^2+(2*r_pinIR)^2)/4 + h_pin^2/3);
J_pin = J_px;

% Load (for weight-shifting)
w_load = 1.5*conv;                  % plate width, [m]
l_load = 3*conv;                    % plate length, [m]
h_load = 8*conv;                    % plate height, [m]
m_load = 10;                        % load mass, [kg]
J_load = 1/12*m_load*(w_load^2 + h_load^2); % Jx

% Motor (Electrical Components)
Ke = 7.2;                           % back emf constant, [V/kRPM]
Kt = 9.7*1.116123;                  % torque constant, [kg-m/amp]
Ra = 0.84;                          % DC armature resistance, [ohms]
La = 0.44;                          % DC armature inductance, [mH]
b = 0.25;                           % viscous friction coefficient, [Nms]

% Motor & Encoder (Mechanical Components)
m_mot = 1.45;                       % motor mass, [kg]
r_mot = 2.05/2*conv;                % motor radius, [m]
h_mot = 6.31*conv+33/1000;          % motor and encoder height [m]
J_motx = 0.5*m_mot*r_mot^2;
J_moty = 1/12*m_mot*(3*r_mot^2 + h_mot^2);
J_mot = J_motx;
eta =0.95;                          % efficiency, assumed
p = 1.5*conv;                       % pitch, [m]
z = 36;                             % number of teeth
% pinion specs: http://www.mcmaster.com/#6325k65/=rxp0m6

%% Parameter Specifications

% Movement Profiles
a  = 2.8;            % acceleration of motors, [m/s^2]
g = 9.81;            % gravity, [m/s^2]
Tw = 1/0.75;         % period of walking/weight-shifting motion, [s/rev]
path = .3048;        % length of track
v_avg = 2*path/Tw;   % average velocity of travel, [m/s]
v_top = 2*v_avg;     % top velocity of travel, [m/s]
a = (v_top)^2/path;  % acceleration, [m/s^2], 2.75 m/s^2
r_pin = r_pinOR;     % radius of pinion, [m] 

% torque due to acceleration
T_ina = (J_mot + J_pin + (m_load+m_rack)/eta*(p^2*z^2)/(4*pi^2))*pi/30*a; 
T = T_ina; 
J = J_pin + J_load + J_mot; 

% Motor Torque Calculations (http://www.leadshine.com/Pdf/Calculation.pdf)
% http://www.orientalmotor.com/technology/articles/motor-sizing-calculations.html
alpha = pi/2;       % anggle of inclination - 90degrees: completely vertical
mu = 1;             % frictional coefficinet of sliding surfaces
% load torque
T_L = ((m_load+m_mot)*g*(2*r_pinOR)*(sin(alpha) + mu*cos(alpha)))/(2*eta); 


%% SIMULATION PARAMETERS
Ts = 0.01; % sampling time
Tf = 5;
tspan = 0:Ts:Tf;
velocity = v_top*sawtooth(2*pi/Tw*tspan, 0.5); 
acceleration = a*square(2*pi/Tw*tspan); 
r_vel = velocity/r_pin;             % velocity/r_pin = r = reference input1
r_accel = acceleration/r_pin;

%% Motor Controller Dynamics
s = tf('s');

% d/dt(omega) = -(b/J)omega + (Kt/J)i + T_d/J;
% d/dt(i) = -(Ke/La)omega - (Ra/La)i + (1/La)Va;

A = [-b/J Kt/J;
    -Ke/La -Ra/La];
B = [0 1/J;
    1/La 0];
C = [1 0];
D = [0 0];

Gsys = ss(A, B, C, D);          % state-space plant model
dcm = feedback(Gsys,Ke,1,1);    % DC motor model (feedback of G and Ke)
%dcm = feedback(Gsys, 1, 1, 1); <-- I THINK THIS IS THE RIGHT ONE

% Plot angular velocity response to a step change in voltage Va
figure(1)
stepplot(dcm(1),'b')
%% Designing a Feedforward Controller
K_ff = 1/dcgain(dcm(1));         % Feedforward gain is inverse of DC gain, 7.22
Td = -J*r_accel;                 % Periodic torque disturbance
t = tspan;
u = [r_vel;Td];                  % input(2)

CL_ff = dcm*diag([K_ff,1]);          
set(CL_ff,'InputName',{'w_ref','Td'},'OutputName','w');

% simulate response of feedforward CL to the triangle-wave input
figure(2)
lsimplot(CL_ff, u, t);          
title('Setpoint Tracking and Disturbance Rejection', 'Interpreter', 'latex', 'FontSize', 10)
legend('Feedforward')
xlabel('Time', 'Interpreter', 'latex', 'FontSize',10)
ylabel('Angular Velocity', 'Interpreter', 'latex', 'FontSize', 10)

%% Feedback Control Design
% Augment the system with an integrator to try to drive the system to zero
% steady-state error. Play around with the feedback gain K and see how it
% affects the plot. 
K = 1.28e2;
K_fb = tf(K,[1 0]);                                 % integrator K/s
CL_int = feedback(dcm*append(K_fb,1),1,1,1);        % integrator CL

% simulate response of feedforward CL with the response of feedback CL
figure(3)
h = lsimplot(CL_ff,CL_int,u,t);
set(CL_int,'InputName',{'w_ref','Td'},'OutputName','w');
title('Setpoint tracking and disturbance rejection', 'Interpreter', 'latex', 'FontSize', 10)
xlabel('Time', 'Interpreter', 'latex', 'FontSize',10)
ylabel('Angular Velocity', 'Interpreter', 'latex', 'FontSize', 10)
legend('Feedforward','Feedback CL','Location','NorthWest')
%% LQR Control Design
% Use a new state vector x = [i; omega] to help synthesize the driving 
% voltage e_A or Va. 
% Va = K1 * omega + K2 * omega/s + K3 * ia
% Cost function to penalize integral error:
DC_aug = [1 ; tf(1,[1 0])] * dcm(1); % add output w/s to DC motor model
K_lqr = lqry(DC_aug,[1 0;0 20000],0.01);

% Next derive the closed-loop model for simulation purposes: 

P = augstate(dcm);                         % inputs:Va,Td  outputs:w,x
C_lqr = K_lqr * append(tf(1,[1 0]),1,1);   % compensator including 1/s
OL = P * append(C_lqr,1);                  % open loop
CL_lqr = feedback(OL,eye(3),1:3,1:3);      % close feedback loops
CL_lqr = CL_lqr(1,[1 4]);                  % extract transfer (w_ref,Td)->w

figure(4)
lsimplot(CL_lqr,u,t)

figure(5)
lsimplot(CL_ff,CL_int,CL_lqr,u,t);
title('Setpoint tracking and disturbance rejection', 'Interpreter', 'latex', 'FontSize', 10)
xlabel('Time', 'Interpreter', 'latex', 'FontSize',10)
ylabel('Angular Velocity', 'Interpreter', 'latex', 'FontSize', 10)
legend('Feedforward','Feedback','Feedback LQR')
%
%This plot compares the closed-loop Bode diagrams for the three designs
figure(6)
bodeplot(CL_ff, CL_int, CL_lqr);
legend('cl\_ff', 'cl\_lqr', 'cl\_lqr');



%% Design Hinf Controller 
s = tf('s');
omega = {10^-3 10^4};
W = [-3,4];w = logspace(W(1),W(2),200); %omega

% Design Criteria
e_T = 0.05; e_TdB = 20*log10(e_T); w_T = 0.1; 
e_nr = 0.1; e_nrdB = 20*log10(e_nr); w_nr = 80;
M_T = 20*log10(1 + 1/e_T);
M_nr = 20*log10(e_nr/(1+e_nr));

k1 = 50;
W1 = k1*(s/(1e-1)+1)^(-1)*(s/1e0+1)^(-2);

figure(7); subplot(2,1,1); 
bodemag(1/W1,w);
title('$e_{T}$ and S(s) Specification', 'Interpreter', 'latex');
xlabel('Frequency', 'Interpreter', 'latex');
ylabel('Magnitude', 'Interpreter', 'latex');

VV=axis; grid on; hold on;
fill([VV(1),VV(1),w_T,w_T,VV(1)],[VV(4),e_TdB,...
	e_TdB,VV(4),VV(4)],[0.5,0,0]); % e_T specification box

% 1/W3
k3 = .02; 
W3 = k3*(s/1.75e0+1)^2/(s/1e2+1)^2;
subplot(2,1,2);
bodemag(1/W3,w);
title('$e_{NR}$ and T(s) Specification', 'Interpreter', 'latex');
xlabel('Frequency', 'Interpreter', 'latex');
ylabel('Magnitude', 'Interpreter', 'latex')
VV=axis; grid on; hold on;
fill([w_nr,w_nr,VV(2),VV(2),w_nr],[VV(4),e_nrdB,...
	e_nrdB,VV(4),VV(4)],[0,0,0.5]); % e_NR specification box

W2 = 0*eye(1);
G = Gsys; Td = -J*r_accel;   

%% Augmented System with Weightings
int = 1/s; %integrator
systemnames = 'G W1 W2 W3 int';
inputvar = '[w{2};u{1};Td{1}]';
outputvar = '[W1; W2; W3; int]';
input_to_G = '[u;Td]';
input_to_W1 = '[w(1)-G(1)]'; 
input_to_W2 = '[u]';
input_to_W3 = '[G(1)]';
input_to_int = '[w(1)-G(1)]';
sysoutname = 'Paug'; cleanupsysic = 'yes'; sysic;
nmeas=2;ncon=2;
[Ka,CLa,GAMa, INFOa] = hinfsyn(ss(Paug), nmeas,ncon,'method','lmi'); % Augmented system
%% Augmented System (Unweighted)

systemnames = 'G int';
inputvar = '[w{2};u{1};Td{1}]';
outputvar = '[w(1)-G(1); u; G(1);int]'; 
input_to_G = '[u;Td]';
input_to_int = '[w(1)-G(1)]';
sysoutname = 'Paug_uw'; cleanupsysic = 'yes'; sysic;

%% Forming the Systems and Plotting
sys_aug = lft(Paug,Ka); %augmented, weighted
sys_aug_uw = lft(Paug_uw,Ka);

[yauw,tauw,xauw] = lsim(sys_aug_uw, u, t);

figure(8)
subplot(2,1,1); hold on;
plot(tauw,u(1,:)'-yauw(:,1),tauw,u(1,:)');
title('Augmented System Response', 'Interpreter', 'latex', 'FontSize', 10);
xlabel('Time [s]', 'Interpreter', 'latex', 'FontSize', 10);
ylabel('Angular Velocity', 'Interpreter', 'latex', 'FontSize', 10)
legend('$\omega(t)$','$\omega_{ref}(t)$')
hold off;

subplot(2,1,2); hold on;
plot(tauw,yauw(:,2),tauw,u(2,:)');
title('Augmented System Response', 'Interpreter', 'latex', 'FontSize', 10);
xlabel('Time [s]', 'Interpreter', 'latex', 'FontSize', 10);
ylabel('Angular Velocity', 'Interpreter', 'latex', 'FontSize', 10)
legend('$\omega(t)$','$T_d(t)$')
hold off;

%% Compare Hinfinity controller to LQR controller
figure(9); hold on;
lsimplot(CL_lqr,u,t);
plot(tauw,u(1,:)'-yauw(:,1),'r');
title('Comparison of Augmented System Responses', 'Interpreter', 'latex', 'FontSize', 10);
xlabel('Time [s]', 'Interpreter', 'latex', 'FontSize', 10);
ylabel('Angular Velocity', 'Interpreter', 'latex', 'FontSize', 10)
legend('LQR Controller', '$H_\infty$ Controller')
hold off;

%% Calculating steady-state errors
[ylqr,tlqr,xlqr]=lsim(CL_lqr,u,t);
ess_lqr = ylqr(1,end)-u(1,end)'
ess_hinf = yauw(1,end)