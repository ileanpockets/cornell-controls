% Eileen J. Kim (ejk93)
% MAE 6780, HW # 3,  Problem # 1
% Start: April 23, 2014
% End: April 24, 2014

%% Problem Set-Up
% Clearing
close all
clear all
clc

% Loading mat-file
load hwp1_data.mat
dt = 0.5; % sample time
n = size(ddCycle,1); % number of samples

% Shifting the data to have a zero mean
ddCycle_detrend = detrend(ddCycle, 'constant');

% Plotting data (with latex font)
figure(1); 
plot(T, ddCycle, 'b', T, ddCycle_detrend, 'g');
xlabel('Time [sec]','Interpreter','latex'); 
ylabel('Speed [mph]','Interpreter','latex'); 
title('EPA Driving Cycle','Interpreter','latex');
legend('Original Data', 'Detrended Data','Interpreter','latex');

%% PART A: Plotting power spectral intensity of the target speed r(t)
% Take Fourier transform of signal
ddCycle_fft = fft(ddCycle_detrend)*dt; % normalize by sampling time
domega = 2*pi/(T(end)); % change in frequency from sampling time
omega = [domega:domega:domega*(n/2)]; % n/2 because of foldback aliasing in discrete FFT
[n,m] = size(ddCycle_fft); Sxx = zeros(n,m,m);

for i = 1:n
    % Apply the Lemma throughout half of the sample
    Sxx(i,:,:) = (ddCycle_fft(i,:))'*ddCycle_fft(i,:)/T(end);
end

% Plotting the power spectral intensity/density
figure(2); 
plot(omega(2:end), Sxx(2:(n/2),1,1)); 
set(gca,'XScale','log','YScale','log'); grid on
xlabel('Frequency [rad/s]','Interpreter','latex'); 
ylabel('Power [PSD]','Interpreter','latex'); 
title('Power Spectral Intensity of Target Speed, r(t)','Interpreter','latex');

%% PART B: Fitting a frequency weighting W_r(s) to unit intensity white noise w(t)
% Simulate white noise 

w = randn(n,1); % white noise
C_ww = cov(w); % covariance, which should equal 1 in this case
% can also use: w = wgn(n,m,0);

% Fit transfer function W_r(s) to part A's graph
s = tf('s'); 
k = 10^(1.8); %gain
Wr = k*(s/(1e-1)+1)^(-2); %Wr
Sxx_model = [];
[RE,IM] = nyquist(Wr,omega); Wr_jw = RE + 1j*IM; % plotting to s = jw

for i = 1:n/2
    Sxx_model(i,:,:) = Wr_jw(:,:,i)*C_ww*Wr_jw(:,:,i)';
end

% Plotting the frequency weighted noise
figure(3); 
loglog(omega(2:end),Sxx(2:(n/2),1,1),omega,Sxx_model(:,1,1)); 
% first data point is a little iffy, starting from the second point
legend('Actual Signal', 'Modeled Signal','Interpreter','latex') 
title('Comparison of Frequency-Weighted Noise Signals','Interpreter','latex')
xlabel('Frequency [rad/s]','Interpreter','latex'); 
ylabel('Power [PDS]','Interpreter','latex');

%% PART C: Comparison of Wr(s) and original data

% Plotting the frequency-weighted noise and the original data on the same
% figure
figure(4);
[z,T,x] = lsim(Wr,w,T);
plot(T,ddCycle_detrend,T,z); 
title('Comparison of Analytical and Simulated','Interpreter','latex')
legend('Detrended Measured Data','Simulated Data','Interpreter','latex')
xlabel('Time [sec]','Interpreter','latex');
ylabel('Speed [mph]','Interpreter','latex');

% Timing of the movement of the analytical and modelled are matching up - we
% expect this because we matched the power density (frequency) curves. THe
% magnitude of the curves don't necessarily match but this is also to be
% expected because the white noise is random and different each time the
% simulation runs. THere are some times when the curves may match better
% than others but it's completely random.