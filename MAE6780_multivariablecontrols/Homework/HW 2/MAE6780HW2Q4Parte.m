function MAE6780HW2Q4Parte
% MAE 6780 HW 2, Q4
% Eileen J. Kim (ejk93)
% March 26, 2014

%% Set-up and Initialization
% Clearing 
close all
clear all
clc

% Original Matrices
A = [0 1 0;
    980 0 -2.8;
    0 0 -100];
B = [0 0 100]';
Cz = [10 1 0; 
    0 0 0];
Cy = [1 0 0];
Dzu = [0 1]';

x0 = [-0.015 0 0]';

%% Continuous-time state feedback Controller
% u(t) = -K1*x(t) - K2*z(t)

% Augmented Matrices
A_til = [A, zeros(3,1); -Cy 0]; %4x4 
B_til = [B; 0]; %4x1
Br_til = [zeros(3,1); 1]; %4x1
[m_b, n_b] = size(B_til); %4x1
x0_til = [x0; 0]; %4x1

%CVX
cvx_begin sdp quiet
    variable Y(m_b,m_b) symmetric %4x4
        variable J(n_b,m_b) %1x4
        variable c1
    (A_til*Y - B_til*J) + (A_til*Y - B_til*J)' <= -c1*eye(size(A_til));
    [Y J'; J eye(n_b)] >= 0;
    [100 x0_til'; x0_til Y] >= 0;
    Y > 0;
    minimize(-c1)
cvx_end

K_til = J/Y; % K_til = [K1 K2]
K1 = K_til(1:3);
K2 = K_til(4);
disp(K_til); % display state-feedback controller
disp(eig(A_til - B_til*K_til)); % display closed loop poles

% Plot response and input
figure(1); clf;
tf = 0.5;
subplot(2,1,1); initial(ss(A_til-B_til*K_til, [], [Cy 0], []),x0_til, tf); ylabel('Response')
subplot(2,1,2); initial(ss(A_til-B_til*K_til, [], [K_til], []), x0_til, tf);ylabel('Input')

%% Observer Controller
% Set-up
[m_b2, n_b2] = size(B); %3x1
sig = 500;

%CVX
cvx_begin sdp quiet
    variable X(m_b2,m_b2) symmetric %3x3
    variable H(m_b2, n_b2) %3x1
    variable c2
    (X*A - H*Cy) + (X*A - H*Cy)' + 2*sig*eye(m_b2)*X <=-c2*eye(size(A));
    X > 0
    minimize(-c2)
cvx_end
L = inv(X)*H; 
disp(L); % display observer controller
disp(eig(A - L*Cy)); % display observer poles
 
% Plot response and input
figure(2); clf;
tf2 = .01;
subplot(2,1,1); initial(ss(A - L*Cy, [], Cy, []), x0, tf2); ylabel('Response')
subplot(2,1,2); initial(ss(A - L*Cy, [], L', []), x0, tf2); ylabel('Input')

%% Simulation of State-Feedback and Observer Controllers
%[x z \bar{x}]' is the new state, A_giant is a 7x7 matrix containing
%state-feedback and observer dynamics
A_giant = zeros(7,7);
A_giant(1:3,1:3) = A-B*K1;
A_giant(1:3,4) = -B*K2;
A_giant(1:3,5:7) = B*K1;
A_giant(4,1:3) = -Cy;
A_giant(4,4) = 0;
A_giant(4,5:7) = zeros(1,3);
A_giant(5:7,1:3) = zeros(3,3);
A_giant(5:7,4) = 0;
A_giant(5:7,5:7) = A - L*Cy;

B_giant = [-L; B; 0];
%Piazza suggested \hat{x} = [0.01 0 0]' so since \bar{x} = x - \hat{x}, the
%initial condition for \bar{x} is [-.01 0 0]', which is shown below,
%concatenated to the initial condition for the augmented matrices
x0_giant = [x0_til;-.01; 0; 0];  

% Plot response and input
figure(3); clf
subplot(2,1,1); initial(ss(A_giant,B_giant,[1 0 0 0 0 0 0],[]),x0_giant, .2); ylabel('\delta y')
subplot(2,1,2); initial(ss(A_giant,B_giant,[K_til,0,0,0],[]),x0_giant); ylabel('Input')
