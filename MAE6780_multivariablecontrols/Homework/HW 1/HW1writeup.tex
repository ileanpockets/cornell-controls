\documentclass[11pt]{article} % use larger type; default would be 10pt

\usepackage[utf8]{inputenc} % set input encoding (not needed with XeLaTeX)

\usepackage[]{mcode} % Allow inclusion of MATLAB code
%%% PAGE DIMENSIONS
\usepackage{geometry}
\geometry{letterpaper}
\geometry{margin=1in}

\usepackage{graphicx}
\usepackage{caption}
\usepackage{subcaption}
\usepackage{float}
\usepackage{amsmath, amssymb,graphics}
 
%%% PACKAGES
\usepackage{booktabs}
\usepackage{array}
\usepackage{paralist} 
\usepackage{verbatim}
\usepackage{subfig}


%%% HEADERS & FOOTERS
\usepackage{fancyhdr} 
\pagestyle{fancy}
\renewcommand{\headrulewidth}{0.2pt} 
\setlength{\headheight}{26pt}
\pagenumbering{arabic}
\lhead{Eileen Kim, ejk93\\
Feb. 14, 2014}
\chead{}
\rhead{\thepage}
\lfoot{}\cfoot{\thepage}\rfoot{}

%%% SECTION TITLE APPEARANCE
\usepackage{sectsty}
\allsectionsfont{\sffamily\mdseries\upshape}

\begin{document}
\begin{center}
\LARGE{\textbf{MAE 6780: Homework 1}}\\
\end{center}

\begin{enumerate}
% Question 1
\item \textbf{Regulating Model Predictive Control}
\begin{enumerate}
% Part A
\item Minimization of J(x,u) with respect to u
	\begin{equation*}
		\begin{split}
		J(x,u) &= x^{*}_{t+N}P_Nx_{t+N} + \sum_{k=t}^{t+N-1}x_k^{*}Qx_k + u_k^{*}Ru_k\\
		&= (S_xx_o+S_uU)^{*}\bar{Q}(S_xx_o+S_uU)+U^{*}\bar{R}U\\
		H&= \bar{R} + S_u^{*}\bar{Q}S_u\\
		P&= Y - KH^{-1}F^{*}\\
		Y &= S_x^{*}\bar{Q}S_x\\
		F &= S_x^{*}\bar{Q}S_u
		\end{split}
	\end{equation*}
	After editing the provided starter code to include the above equations, I was able to run the code to observe the function at different sampling rates and prediction horizons. For example, the Batch method applied to the function for a sampling rate of 0.01s and a horizon length of 0.2 is shown below:
	\begin{figure}[H]
	\centering
	\includegraphics[scale = 0.8, trim = 1.5in 3in 1.5in 3.5in, clip]{ts01tp2}
	\caption{Batch Method for $\mathrm{T}_\mathrm{s} = 0.01 \mathrm{s}$ and $\mathrm{T}_\mathrm{p} = 0.2$}
	\end{figure}
	The results of running the Batch Method at all the desired sampling rates and prediction lengths are summarized in the table below:
	\begin{center}
			\begin{tabular}{|c | c | c | c | c | c |}
			\hline
			 &  $T_p = 0.1$ & $T_p = 0.14$ & $T_p = 0.15$ & $T_p = 0.16$ & $T_p = 0.2$\\
			\hline
			$T_s = 0.01$ & unstable & stable & stable & - & stable\\
			\hline
			$T_s = 0.02$ & unstable & stable & - & stable & stable\\
			\hline
			$T_s = 0.05$ & unstable & - & unstable & - & stable\\
			\hline
			\end{tabular}
	\end{center}
	As the sampling rates get smaller, the graphs become more obviously discrete.
	
	% Part B
	\item Recursive (Dynamic Programming) Approach
	The results of the recursive approach are displayed below:
	\begin{center}
				\begin{tabular}{|c | c | c | c | c | c |}
				\hline
				 &  $T_p = 0.1$ & $T_p = 0.14$ & $T_p = 0.15$ & $T_p = 0.16$ & $T_p = 0.2$\\
				\hline
				$T_s = 0.01$ & unstable & stable & stable & - & stable\\
				\hline
				$T_s = 0.02$ & unstable & stable & - & stable & stable\\
				\hline
				$T_s = 0.05$ & unstable & - & unstable & - & stable\\
				\hline
				\end{tabular}
		\end{center}
		
		The results from the dynamic programming and batch approach are exactly the same. This proves that they are both valid methods to calculating the feedback gain and control on minimizing cost functions. In this case, especially because we are using MATLAB and small matrices, there also doesn't seem to be a difference in evaluation time. However, as we start to model programs which have much larger matrices, it's likely that the dynamic programming approach will become a more valuable method of solving this problem. This is because it doesn't involve inversing large matrices. 
	\end{enumerate}
	
% Question 2
\item \textbf{Proportional and Integral Tracking Model Predictive Control}
\begin{enumerate}
% Part A
\item Augmented System
\begin{enumerate}
% Part i
\item Rewriting the cost function $J(\tilde{x},u)$ explicitly as a function of $(\tilde{x}, u)$:
\begin{equation*}
\begin{split}
	J(\tilde{x},u) &= e_{k+N}^{*}P_{e,N}e_{k+N} + \sum_{i=k}^{k+N-1}e_i^{*}Q_ee_i + u_i^{*}Ru_i\\
	&= (\tilde{C}\tilde{x}_{k+N})^{*}P_{e,N}(\tilde{C}\tilde{x}_{k+N}) + \sum_{i = k}^{k+N-1}(\tilde{C}\tilde{x}_i)^{*}Q_e(\tilde{X}\tilde{x}_i) + u_i^{*}Ru
\end{split}
\end{equation*}
\item Simulating and plotting using the following augmented matrices:\\
\begin{lstlisting}
A_tilde = [Gdsys.A [0; 0; 0]; [0 0 0 eye(1)]];
B_tilde = [Gdsys.B; 0];
C_tilde = [1 0 0 -1];
\end{lstlisting} 
Below is the resulting set of plots. Please see published code in the zipped folder.
	\begin{figure}[H]
	\centering
	\includegraphics[scale = 0.7, trim = 1.5in 3in 1.5in 3.5in, clip]{2aii}
	\caption{Recursive Method with Augmented Systems}
	\end{figure}
With a $\rho = 1$ there is a steady state error of -0.02. 
\item As $\rho$ is increased, it is pushed into instability around $\rho = 1e8$, with a large error, as observed by the figure below. 
	\begin{figure}[H]
	\centering
	\includegraphics[scale = 0.7, trim = 1.5in 3in 1.5in 3.5in, clip]{2aiii1}
	\caption{Recursive Method with Augmented Systems, $\rho = 1e8$}
	\end{figure}
	
As $\rho$ is decreased to smaller values, even smaller steady-state errors are observed, but the system never reaches zero, as is observed in the figure below at a $\rho = 1e-20$:
 	\begin{figure}[H]
 	\centering
 	\includegraphics[scale = 0.7, trim = 1.5in 3in 1.5in 3.5in, clip]{2aiii2}
 	\caption{Recursive Method with Augmented Systems, $\rho = 1e-20$}
 	\end{figure}
 	
In fact, once $\rho$ is decreased to 1e-200, the figure below results with a steady state error back to the original -0.02, which was observed at $\rho = 1$.
	\begin{figure}[H]
 	\centering
 	\includegraphics[scale = 0.7, trim = 1.5in 3in 1.5in 3.5in, clip]{2aiii3}
 	\caption{Recursive Method with Augmented Systems, $\rho = 1e-200$}
 	\end{figure}
 	
\end{enumerate}
\item Smarty apparently doesn't like my controller, so we'll do some steady-state analysis
\begin{enumerate}
\item Deriving an expression to relate steady-state $x_{\infty}$ to steady-state input $u_{\infty}$\\
At steady-state, $\dot{x}(t) = 0$
\begin{equation*}
\begin{split}
0 &= Ax(t) + Bu(t)\\
&= Ax_{\infty}+ Bu_{\infty}\\
Ax_{\infty} &= -Bu_{\infty}\\
x_{\infty} &= A^{-1}Bu_{\infty}\\
\Phi_{xu} &= -A^{-1}B
\end{split}
\end{equation*}
\item Solving for the minimizing $u_{\infty}$
\begin{equation*}
\begin{split}
J(x,u) &= e_{k+1}^{*}P_{e,N}e_{k+1} + \sum_{i=k}^{k}(Cx_i - r)^{*}Q_e(Cx_i -r) + u_i^{*}Ru_i\\
&= e_{\infty}^2 + \sum(Cx_i - r)^2 + u^2\\
&= 2e_{\infty}^2 + u_{\infty}^2\\
&= 2\left(Cx_{\infty}-r\right)^2 + u_{\infty}^{*}Ru_{\infty}\\
&= 2\left[(C\Phi_{xu}u_{\infty})^2 - 2C\Phi_{xu}u_{\infty}r + r^2\right] + u_{\infty}^{*}Ru_{\infty}
\end{split}
\end{equation*}
Set the derivative to 0. 
\begin{equation*}
\begin{split}
\frac{\partial J}{\partial u_{\infty}} &= 4(C\Phi_{xu}^2)u_{\infty} - 4C\Phi_{xu}r + 2u_{\infty}^{*}R = 0\\
u_{\infty}\left[2\Phi_{xu}^{*}C^{*}C\Phi_{xu} + R\right] &= 2C\Phi_{xu}r\\
u_{\infty} &= (2\Phi_{xu}^{*}C^{*}C\Phi_{xu} + R)^{-1}C\Phi_{xu}r
\end{split}
\end{equation*}

\item Steady-state error
\begin{equation*}
\begin{split}
e_{\infty}&= C\Phi_{xu}\Phi_{ur}r -r\\
&=-CA^{-1}B(2\Phi_{xu}^{*}C^{*}C\Phi_{xu} + R)^{-1}C(-A^{-1}B)r -r 
\end{split}
\end{equation*}
\end{enumerate}

\item Adding an integrator to help step reference produces the following plot:
	\begin{figure}[H]
 	\centering
 	\includegraphics[scale = 0.7, trim = 1.5in 3in 1.5in 3.5in, clip]{2c}
 	\caption{Recursive Method with Augmented System with Integrator}
 	\end{figure}
 	Assuming the feedback control law is stabilizing, the steady-state error goes to 0 as k goes to infinity because adding an integrator to the system cancels out the step reference input. The types of the system and the control law match up. 
\end{enumerate}
\end{enumerate}
\end{document}